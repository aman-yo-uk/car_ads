Project for submitting advertisements for the sale of cars

USE:
- ASP.Net Core Web API
- PostgreSQL
- ReactJS
- Repository Pattern
- Unit Of Work
- Dependency Injection
- Entity Framework Core
- Web Integration Tests
- Swagger