﻿using Aman.Tools.Extensions;
using Aman.Tools.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

namespace Aman.CarAds.Core.Resources
{
    public static class Resourcer
    {
        private static readonly Dictionary<string, dynamic> ResFiles = new Dictionary<string, dynamic>();
        private static readonly Dictionary<string, FileSystemWatcher> FileWatchers = new Dictionary<string, FileSystemWatcher>();
        private static ResourceNotFoundBehaviorEnum _notFoundBehavior = ResourceNotFoundBehaviorEnum.ReturnResourceKey;

        private static void LoadResFile(string path)
        {
            if (File.Exists(path))
            {
                if (!ResFiles.ContainsKey(path))
                {
                    ResFiles.Add(path, null);
                }

                var resContent = FileHelper.ReadContent(path)
                    .Replace("\t", "")
                    .Replace("\n", "")
                    .Replace("\r", "");
                ResFiles[path] = JsonConvert.DeserializeObject(resContent);
            }
        }

        private static void AddFileWatcher(string path)
        {
            if (File.Exists(path))
            {
                var dirPath = Path.GetDirectoryName(path);
                var fileName = Path.GetFileName(path);

                if (FileWatchers.ContainsKey(path))
                {
                    FileWatchers[path]?.Dispose();
                    FileWatchers[path] = null;
                }
                else
                {
                    FileWatchers.Add(path, null);
                }

                FileSystemWatcher watcher = new FileSystemWatcher();
                watcher.Path = dirPath;
                watcher.Filter = fileName;
                watcher.NotifyFilter = NotifyFilters.LastWrite;
                watcher.Changed += new FileSystemEventHandler((o, a) => { LoadResFile(a.FullPath); });
                watcher.EnableRaisingEvents = true;

                FileWatchers[path] = watcher;
            }
        }

        public static void Init(bool needWatcher, ResourceNotFoundBehaviorEnum behavior, params string[] paths)
        {
            Dispose();
            _notFoundBehavior = behavior;
            foreach (var path in paths)
            {
                LoadResFile(path);
                if (needWatcher)
                    AddFileWatcher(path);
            }
        }

        public static void Dispose()
        {
            if (FileWatchers != null)
            {
                foreach (var fileWatcher in FileWatchers)
                {
                    fileWatcher.Value?.Dispose();
                }
                FileWatchers.Clear();
            }

            ResFiles.Clear();
        }

        /// <summary>
        /// Получить значение ресурса из региона по ключу и культуре
        /// </summary>
        /// <param name="regionName"></param>
        /// <param name="keyName"></param>
        /// <param name="cultureName"></param>
        /// <param name="behavior"></param>
        /// <returns></returns>
        public static string Get(string regionName, string keyName, string cultureName, ResourceNotFoundBehaviorEnum behavior = ResourceNotFoundBehaviorEnum.Undefined)
        {
            foreach (var resFile in ResFiles)
            {
                dynamic resData = resFile.Value;
                if (resData != null)
                {
                    var res = regionName.HasValue()
                        ? resData[regionName]?[keyName]?[cultureName] :
                        resData[keyName]?[cultureName];
                    if (res != null)
                    {
                        var resStr = res.ToString();
                        if (!string.IsNullOrEmpty(resStr))
                            return res.ToString();
                    }
                }
            }
            var key = regionName.HasValue()
                    ? $"{regionName}.{keyName}.{cultureName}"
                    : $"{keyName}.{cultureName}";

            var notFoundBehavior = behavior != ResourceNotFoundBehaviorEnum.Undefined ? behavior : _notFoundBehavior;
            return notFoundBehavior switch
            {
                ResourceNotFoundBehaviorEnum.ReturnNull => null,
                ResourceNotFoundBehaviorEnum.ReturnThrow => throw new Exception($"Не задано значение ресурса [{key}]"),
                _ => key,
            };
        }

        /// <summary>
        /// Получить значение ресурса по ключу и культуре
        /// </summary>
        /// <param name="keyName"></param>
        /// <param name="cultureName"></param>
        /// <param name="behavior"></param>
        /// <returns></returns>
        public static string Get(string keyName, string cultureName, ResourceNotFoundBehaviorEnum behavior = ResourceNotFoundBehaviorEnum.Undefined)
        {
            return Get(null, keyName, cultureName, behavior);
        }

        /// <summary>
        /// Получить значение ресурса для значения Enum по культуре
        /// </summary>
        /// <param name="enumValue"></param>
        /// <param name="cultureName"></param>
        /// <returns></returns>
        public static string Get(Enum enumValue, string cultureName)
        {
            var regionName = enumValue.GetType().Name.Split('.').LastOrDefault();
            var keyName = enumValue.ToString();
            return Get(regionName, keyName, cultureName);
        }

        /// <summary>
        /// Получить словарь значениий ресурсов для нескольких значений Enum по культуре
        /// </summary>
        /// <param name="cultureName"></param>
        /// <param name="enumValues"></param>
        /// <returns></returns>
        public static Dictionary<int, string> Get(string cultureName, params Enum[] enumValues)
        {
            var dict = new Dictionary<int, string>();
            foreach (var enumValue in enumValues)
            {
                dict.Add(Convert.ToInt32(enumValue), Get(enumValue, cultureName));
            }
            return dict;
        }

        /// <summary>
        /// Получить словарь значениий ресурсов для нескольких значений Enum для текущей культуры
        /// </summary>
        /// <param name="enumValues"></param>
        /// <returns></returns>
        public static Dictionary<int, string> GetCurrent(params Enum[] enumValues)
        {
            return Get(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName, enumValues);
        }

        /// <summary>
        /// Получить словарь значениий ресурсов для всех значений Enum по культуре
        /// </summary>
        /// <param name="cultureName"></param>
        /// <param name="enumValue"></param>
        /// <returns></returns>
        public static Dictionary<int, string> GetAll(string cultureName, Enum enumValue)
        {
            var dict = new Dictionary<int, string>();
            var enumValues = Enum.GetValues(enumValue.GetType());
            foreach (var value in enumValues)
            {
                dict.Add(Convert.ToInt32(value), Get((Enum)value, cultureName));
            }
            return dict;
        }

        /// <summary>
        /// Получить словарь значениий ресурсов для всех значений Enum для текущей культуры
        /// </summary>
        /// <param name="enumValue"></param>
        /// <returns></returns>
        public static Dictionary<int, string> GetAllCurrent(Enum enumValue)
        {
            return GetAll(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName, enumValue);
        }
    }
}