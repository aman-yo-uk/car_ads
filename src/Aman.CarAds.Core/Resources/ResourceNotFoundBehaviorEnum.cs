﻿namespace Aman.CarAds.Core.Resources
{
    public enum ResourceNotFoundBehaviorEnum
    {
        Undefined = 0,
        ReturnResourceKey = 1,
        ReturnNull = 2,
        ReturnThrow = 3
    }
}