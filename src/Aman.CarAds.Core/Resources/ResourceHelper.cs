﻿using Aman.Tools.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace Aman.CarAds.Core.Resources
{
    public static class ResourceHelper
    {
        public const string Kk = "kk";
        public const string Ru = "ru";
        public const string En = "en";

        public static string GetEnumDirResource(Enum @enum, string culture = null)
        {
            return Resourcer.Get(@enum, culture ?? Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName);
        }

        public static Dictionary<int, string> GetDictionary(Enum @enum, string culture = null)
        {
            var result = culture.HasValue() ? Resourcer.GetAll(culture, @enum) : Resourcer.GetAllCurrent(@enum);
            return result;
        }

        public static Dictionary<int, string> GetDictionary(string culture = null, params Enum[] enumValues)
        {
            var result = culture.HasValue() ? Resourcer.Get(culture, enumValues) : Resourcer.GetCurrent(enumValues);
            return result;
        }

        public static string GetByCodeFromRegion(string region, string code, string culture = null, ResourceNotFoundBehaviorEnum behavior = ResourceNotFoundBehaviorEnum.Undefined)
        {
            if (!culture.HasValue())
                culture = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;

            return Resourcer.Get(region, code, culture, behavior);
        }

        public static string GetByCode(string code, string culture = null, ResourceNotFoundBehaviorEnum behavior = ResourceNotFoundBehaviorEnum.Undefined)
        {
            if (!culture.HasValue())
                culture = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
            return Resourcer.Get(code, culture, behavior);
        }

        public static void Init(ResourceNotFoundBehaviorEnum behavior, bool needWatcher = false)
        {
            var appPath = AppDomain.CurrentDomain.BaseDirectory;

            Resourcer.Init(needWatcher, behavior,
                Path.Combine(appPath, "Resources", "resources.json"),
                Path.Combine(appPath, "Resources", "resources-local.json")
            );

            //Проверка на правильную инициализацию ресурсов
            Resourcer.Get("UserTypeEnum", "Admin", "ru", ResourceNotFoundBehaviorEnum.ReturnThrow);
            Resourcer.Get("CityName", "ru", ResourceNotFoundBehaviorEnum.ReturnThrow);
        }
    }
}