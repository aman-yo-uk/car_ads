﻿using Aman.CarAds.Dtos.Base;
using Aman.CarAds.Dtos.User;
using System.Threading.Tasks;

namespace Aman.CarAds.Services
{
    public interface IUserService
    {
        Task<Response<UserProfileDto>> GetProfileAsync();
        Task<Response<bool>> AddModeratorAsync(ModeratorInfoDto moderatorInfo);
        Task<Response<bool>> EditUserAsync(UserInfoDto userInfo);
        Task<Response<bool>> ChangeSecureKeyAsync(ChangeSecureKeyDto changeSecureKey);
        Task<Response<bool>> BanUserAsync(BanUserDto banUserInfo);
        Task<Response<bool>> DeleteAsync(long id);
    }
}