﻿using Aman.CarAds.Dtos.Ad;
using Aman.CarAds.Dtos.Base;
using Aman.CarAds.Dtos.File;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Aman.CarAds.Services
{
    public interface IAdService
    {
        Task<Response<List<AdListInfoDto>>> GetListAsync(RequestAdInfosDto requestAdListInfo);
        Task<Response<AdInfoDto>> GetByIdAsync(long id);
        Task<Response<List<UserAdInfosDto>>> GetUserListAsync(AdStateEnum adState);
        Task<Response<bool>> AddAsync(NewAdInfoDto adInfo);
        Task<Response<bool>> EditAdAsync(EditAdInfoDto adInfo);
        Task<Response<bool>> EditPhotosAsync(EditAdPhotosInfoDto adInfo);
        Task<Response<bool>> DeleteAsync(long id);
        Task<Response<bool>> ArchivateAsync(long id);
        Task<Response<bool>> ActivateAsync(long id);
        Task<Response<bool>> DeactivateAsync(AdDeactivateInfoDto adDeactivateInfo);
    }
}