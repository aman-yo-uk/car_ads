﻿using Aman.CarAds.Dtos.Base;
using Aman.CarAds.Dtos.User;
using Aman.CarAds.Dtos.UserAuth;
using System.Threading.Tasks;

namespace Aman.CarAds.Services
{
    public interface IUserAuthService
    {
        Task<Response<UserAuthInfoDto>> LoginAsync(LoginUserInfoDto loginUserInfo);
        Task<Response<UserAuthInfoDto>> RegisterAsync(RegisterUserInfoDto userInfo);
    }
}