﻿using Aman.CarAds.Core.Resources;
using Aman.CarAds.Dtos;
using Aman.Tools.Extensions;
using Microsoft.AspNetCore.Http;
using System.Globalization;
using System.Threading;

namespace Aman.CarAds.Services.Base
{
    public class LangService : BaseService
    {
        private LangEnum? _lang;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public LangService(IHttpContextAccessor httpContextAccessor) : base (httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            Thread.CurrentThread.CurrentCulture = new CultureInfo(Culture);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(Culture);
        }

        public LangEnum Lang
        {
            get
            {
                if (_lang == null)
                {
                    var lang = _httpContextAccessor.HttpContext.Request != null && _httpContextAccessor.HttpContext.Request.Headers["lang"].ToString().HasValue()
                        ? _httpContextAccessor.HttpContext.Request?.Headers["lang"].ToString()
                        : CultureInfo.CurrentUICulture.TwoLetterISOLanguageName;

                    _lang = lang switch
                    {
                        "ru" => LangEnum.Rus,
                        "en" => LangEnum.Eng,
                        _ => LangEnum.Kaz,
                    };
                }
                return _lang.Value;
            }
        }

        public string Culture
        {
            get
            {
                return GetCulture(Lang);
            }
        }

        private string GetCulture(LangEnum lang)
        {
            if (lang == LangEnum.Undefined)
            {
                lang = Lang;
            }
            return lang == LangEnum.Rus ? ResourceHelper.Ru : lang == LangEnum.Eng ? ResourceHelper.En : ResourceHelper.Kk;
        }

        public string GetServiceResource(string code, LangEnum lang = LangEnum.Undefined)
        {
            return ResourceHelper.GetByCodeFromRegion(this.GetType().Name, code, GetCulture(lang));
        }
    }
}