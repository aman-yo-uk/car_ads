﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IO;
using System.Text;

namespace Aman.CarAds.Services.Base
{
    public class AuthService
    {
        private static readonly IConfigurationRoot Configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json")
            .Build();

        private static readonly string Key = Configuration.GetSection("Jwt:Key").Value;
        public static readonly string Issuer = Configuration.GetSection("Jwt:Issuer").Value;
        public static readonly string Audience = Configuration.GetSection("Jwt:Audience").Value;
        public static readonly int Lifetime = Convert.ToInt32(Configuration.GetSection("Jwt:Lifetime").Value);
        public static readonly bool ValidateIssuer = Convert.ToBoolean(Configuration.GetSection("Jwt:ValidateIssuer").Value);
        public static readonly bool ValidateAudience = Convert.ToBoolean(Configuration.GetSection("Jwt:ValidateAudience").Value);
        public static readonly bool ValidateLifetime = Convert.ToBoolean(Configuration.GetSection("Jwt:ValidateLifetime").Value);
        public static readonly bool ValidateIssuerSigningKey = Convert.ToBoolean(Configuration.GetSection("Jwt:ValidateIssuerSigningKey").Value);

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Key));
        }
    }
}