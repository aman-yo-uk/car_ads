﻿using Aman.CarAds.Dtos.User;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;

namespace Aman.CarAds.Services.Base
{
    public class BaseService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public BaseService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public bool IsAuthenticated
        {
            get
            {
                return _httpContextAccessor?.HttpContext?.User?.Identity?.IsAuthenticated ?? false;
            }
        }

        public long? UserId
        {
            get
            {
                var userIdStr = _httpContextAccessor?.HttpContext?.User?.Claims?.FirstOrDefault(_ => _.Type == "UserId")?.Value;
                var userIdRes = long.TryParse(userIdStr, out long userId);
                return userIdRes ? userId : (long?)null;
            }
        }

        public string Login
        {
            get
            {
                return _httpContextAccessor?.HttpContext?.User?.Claims?.FirstOrDefault(_ => _.Type == "Login")?.Value;
            }
        }

        public UserTypeEnum? UserType
        {
            get
            {
                var userTypeStr = _httpContextAccessor?.HttpContext?.User?.Claims?.FirstOrDefault(_ => _.Type == "UserType")?.Value;
                var userTypeRes = Enum.TryParse(userTypeStr, out UserTypeEnum userType);
                return userTypeRes ? userType : (UserTypeEnum?)null;
            }
        }
    }
}