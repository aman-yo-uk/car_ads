﻿using Aman.CarAds.Core.Resources;
using Aman.CarAds.Dal.Db.Base;
using Aman.CarAds.Dal.Db.CarAds.Models;
using Aman.CarAds.Dal.Db.CarAds.Repositories;
using Aman.CarAds.Dtos.Base;
using Aman.CarAds.Dtos.Email;
using Aman.CarAds.Dtos.User;
using Aman.CarAds.Services.Base;
using Aman.Tools.Extensions;
using Aman.Tools.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Aman.CarAds.Services.Impl
{
    public class UserService : LangService, IUserService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IUow<CarAdsContext> _uow;
        private readonly IUserRepository _userRepository;
        private readonly IEmailRepository _emailRepository;

        public UserService(
            IHttpContextAccessor httpContextAccessor,
            IUow<CarAdsContext> uow,
            IUserRepository userRepository,
            IEmailRepository emailRepository
            ) : base(httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _uow = uow;
            _userRepository = userRepository;
            _emailRepository = emailRepository;
        }

        public async Task<Response<UserProfileDto>> GetProfileAsync()
        {
            if (!_httpContextAccessor.HttpContext.User.Identity.IsAuthenticated)
                return Response<UserProfileDto>.GetError(GetServiceResource("Unauthorized"));

            var login = _httpContextAccessor.HttpContext.User.Identity.Name;

            var user = await _userRepository.GetDtoQuery()
                .FirstOrDefaultAsync(_ => _.Login == login);

            if (user == null)
                return Response<UserProfileDto>.GetError(GetServiceResource("UserNotFound"));

            var res = new UserProfileDto
            {
                Login = user.Login,
                Nickname = user.Nickname,
                BirthDate = user.BirthDate,
                GenderType = user.GenderType,
            };

            return Response<UserProfileDto>.GetSuccess(res);
        }

        public async Task<Response<bool>> AddModeratorAsync(ModeratorInfoDto moderatorInfo)
        {
            if (moderatorInfo == null)
                return Response<bool>.GetError(GetServiceResource("NoDataReceived"));

            if (moderatorInfo.Login.IsNullOrEmpty())
                return Response<bool>.GetError(ResourceHelper.GetByCodeFromRegion("UserAuthService", "LoginIsEmpty", Culture));

            if (moderatorInfo.SecureKey.IsNullOrEmpty())
                return Response<bool>.GetError(ResourceHelper.GetByCodeFromRegion("UserAuthService", "SecureKeyIsEmpty", Culture));

            var formatEmailRes = FormatEmailHelper.FormatEmail(moderatorInfo.Login);
            if (!formatEmailRes.IsSuccess)
                return Response<bool>.GetError(ResourceHelper.GetEnumDirResource(formatEmailRes.FormatEmailError, Culture));

            var clearEmail = formatEmailRes.Email;

            if (await _userRepository.GetDtoQuery().AnyAsync(_ => _.Login == moderatorInfo.Login || _.Login == clearEmail))
                return Response<bool>.GetError(ResourceHelper.GetByCodeFromRegion("UserAuthService", "LoginIsTaken", Culture));

            var secureKeyHash = CryptHelper.GetMD5(moderatorInfo.SecureKey);

            var user = new UserDto
            {
                Login = clearEmail,
                SecureKeyHash = secureKeyHash,
                Type = UserTypeEnum.Moderator
            };

            var email = new EmailDto
            {
                UserId = user.Id,
                EmailAddress = clearEmail
            };

            using (var tran = _uow.BeginTransaction())
            {
                await _userRepository.SaveDtoAsync(user);
                await _emailRepository.SaveDtoAsync(email);

                return Response<bool>.GetSuccess(true);
            }
        }

        public async Task<Response<bool>> EditUserAsync(UserInfoDto userInfo)
        {
            if (!IsAuthenticated)
                return Response<bool>.GetError(GetServiceResource("Unauthorized"));

            var user = await _userRepository.GetDtoByIdAsync(UserId.Value);

            if (user == null)
                return Response<bool>.GetError(GetServiceResource("UserNotFound"));

            user.Nickname = userInfo.Nickname;
            user.BirthDate = userInfo.BirthDate;
            user.GenderType = userInfo.GenderType;

            await _userRepository.SaveDtoAsync(user);

            return Response<bool>.GetSuccess(true);
        }

        public async Task<Response<bool>> ChangeSecureKeyAsync(ChangeSecureKeyDto changeSecureKey)
        {
            if (!IsAuthenticated)
                return Response<bool>.GetError(GetServiceResource("Unauthorized"));

            if (changeSecureKey == null)
                return Response<bool>.GetError(ResourceHelper.GetByCodeFromRegion("UserAuthService", "LoginOrSecureKeyIncorrect", Culture));

            if (changeSecureKey.SecureKey.IsNullOrEmpty())
                return Response<bool>.GetError(ResourceHelper.GetByCodeFromRegion("UserAuthService", "SecureKeyIsEmpty", Culture));

            if (changeSecureKey.SecureKeyRepeat.IsNullOrEmpty())
                return Response<bool>.GetError(ResourceHelper.GetByCodeFromRegion("UserAuthService", "SecureKeyRepeatIsEmpty", Culture));

            if (changeSecureKey.SecureKey != changeSecureKey.SecureKeyRepeat)
                return Response<bool>.GetError(ResourceHelper.GetByCodeFromRegion("UserAuthService", "PasswordAndPasswordRepeatNotMatch", Culture));

            var user = await _userRepository.GetDtoByIdAsync(UserId.Value);

            if (user == null)
                return Response<bool>.GetError(GetServiceResource("UserNotFound"));

            var secureKeyHash = CryptHelper.GetMD5(changeSecureKey.SecureKey);

            user.SecureKeyHash = secureKeyHash;
            await _userRepository.SaveDtoAsync(user);

            return Response<bool>.GetSuccess(true);
        }

        public async Task<Response<bool>> BanUserAsync(BanUserDto banUserInfo)
        {
            if (!IsAuthenticated)
                return Response<bool>.GetError(GetServiceResource("Unauthorized"));

            var user = await _userRepository.GetDtoByIdAsync(banUserInfo.UserId);

            if (user == null)
                return Response<bool>.GetError(GetServiceResource("UserNotFound"));

            user.BannedTo = banUserInfo.BannedTo;
            user.BannedReason = banUserInfo.BannedReason;

            await _userRepository.SaveDtoAsync(user);

            return Response<bool>.GetSuccess(true);
        }

        public async Task<Response<bool>> DeleteAsync(long id)
        {
            if (!IsAuthenticated)
                return Response<bool>.GetError(GetServiceResource("Unauthorized"));

            var user = await _userRepository.GetDtoByIdAsync(id);

            if (user == null)
                return Response<bool>.GetError(GetServiceResource("UserNotFound"));

            await _userRepository.DeleteDtoAsync(user);

            return Response<bool>.GetSuccess(true);
        }
    }
}