﻿using Aman.CarAds.Core.Resources;
using Aman.CarAds.Dal.Db.Base;
using Aman.CarAds.Dal.Db.CarAds.Models;
using Aman.CarAds.Dal.Db.CarAds.Repositories;
using Aman.CarAds.Dtos.Base;
using Aman.CarAds.Dtos.Email;
using Aman.CarAds.Dtos.User;
using Aman.CarAds.Dtos.UserAuth;
using Aman.CarAds.Services.Base;
using Aman.Tools.Extensions;
using Aman.Tools.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Aman.CarAds.Services.Impl
{
    public class UserAuthService : LangService, IUserAuthService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IUow<CarAdsContext> _uow;
        private readonly IUserRepository _userRepository;
        private readonly IUserAuthRepository _userAuthRepository;
        private readonly IEmailRepository _emailRepository;

        public UserAuthService(
            IHttpContextAccessor httpContextAccessor,
            IUow<CarAdsContext> uow,
            IUserRepository userRepository,
            IUserAuthRepository userAuthRepository,
            IEmailRepository emailRepository
            ) : base (httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _uow = uow;
            _userRepository = userRepository;
            _userAuthRepository = userAuthRepository;
            _emailRepository = emailRepository;
        }

        private string GetToken(ClaimsIdentity identity)
        {
            var now = DateTime.Now;

            var jwt = new JwtSecurityToken(
                issuer: AuthService.Issuer,
                audience: AuthService.Audience,
                notBefore: now,
                claims: identity.Claims,
                expires: now.Add(TimeSpan.FromMinutes(AuthService.Lifetime)),
                signingCredentials: new SigningCredentials(AuthService.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));

            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }

        private ClaimsIdentity GetIdentity(UserDto user)
        {
            if (user == null)
            {
                return null;
            }

            var claims = new List<Claim>
            {
                new Claim("UserId", user.Id.ToString()),
                new Claim("Login", user.Login),
                new Claim("UserType", user.Type.ToString())
            };

            var claimsIdentity = new ClaimsIdentity(claims, "Token");
            return claimsIdentity;
        }

        public async Task<Response<UserAuthInfoDto>> LoginAsync(LoginUserInfoDto loginUserInfo)
        {
            if (loginUserInfo == null)
                return Response<UserAuthInfoDto>.GetError(GetServiceResource("LoginOrSecureKeyIncorrect"));

            if (loginUserInfo.Login.IsNullOrEmpty())
                return Response<UserAuthInfoDto>.GetError(GetServiceResource("LoginIsEmpty"));

            if (loginUserInfo.SecureKey.IsNullOrEmpty())
                return Response<UserAuthInfoDto>.GetError(GetServiceResource("SecureKeyIsEmpty"));

            if (loginUserInfo.IpAddress.IsNullOrEmpty())
                return Response<UserAuthInfoDto>.GetError(GetServiceResource("IpAddressIsEmpty"));

            var secureKeyHash = CryptHelper.GetMD5(loginUserInfo.SecureKey);

            var user = await _userRepository.GetDtoQuery()
                .FirstOrDefaultAsync(_ => _.Login == loginUserInfo.Login && _.SecureKeyHash == secureKeyHash);

            if (user == null)
                return Response<UserAuthInfoDto>.GetError(GetServiceResource("LoginOrSecureKeyIncorrect"));

            var identity = GetIdentity(user);
            var token = GetToken(identity);

            var userAuth = new UserAuthDto
            {
                UserId = user.Id,
                LoginDateTime = DateTime.Now,
                Token = token,
                IpAddress = loginUserInfo.IpAddress
            };
            await _userAuthRepository.SaveDtoAsync(userAuth);

            var res = new UserAuthInfoDto
            {
                Token = token
            };

            return Response<UserAuthInfoDto>.GetSuccess(res);
        }

        public async Task<Response<UserAuthInfoDto>> RegisterAsync(RegisterUserInfoDto userInfo)
        {
            if (userInfo == null)
                return Response<UserAuthInfoDto>.GetError(GetServiceResource("LoginOrSecureKeyIncorrect"));

            if (userInfo.Login.IsNullOrEmpty())
                return Response<UserAuthInfoDto>.GetError(GetServiceResource("LoginIsEmpty"));

            if (userInfo.SecureKey.IsNullOrEmpty())
                return Response<UserAuthInfoDto>.GetError(GetServiceResource("SecureKeyIsEmpty"));

            if (userInfo.SecureKeyRepeat.IsNullOrEmpty())
                return Response<UserAuthInfoDto>.GetError(GetServiceResource("SecureKeyRepeatIsEmpty"));

            if (userInfo.SecureKey != userInfo.SecureKeyRepeat)
                return Response<UserAuthInfoDto>.GetError(GetServiceResource("PasswordAndPasswordRepeatNotMatch"));

            if (userInfo.IpAddress.IsNullOrEmpty())
                return Response<UserAuthInfoDto>.GetError(GetServiceResource("IpAddressIsEmpty"));

            var formatEmailRes = FormatEmailHelper.FormatEmail(userInfo.Login);
            if (!formatEmailRes.IsSuccess)
                return Response<UserAuthInfoDto>.GetError(ResourceHelper.GetEnumDirResource(formatEmailRes.FormatEmailError, Culture));

            var clearEmail = formatEmailRes.Email;

            if (await _userRepository.GetDtoQuery().AnyAsync(_ => _.Login == userInfo.Login || _.Login == clearEmail))
                return Response<UserAuthInfoDto>.GetError(GetServiceResource("LoginIsTaken"));

            var secureKeyHash = CryptHelper.GetMD5(userInfo.SecureKey);

            var user = new UserDto
            {
                Login = clearEmail,
                SecureKeyHash = secureKeyHash,
                Type = UserTypeEnum.User
            };

            var email = new EmailDto
            {
                UserId = user.Id,
                EmailAddress = clearEmail
            };

            using (var tran = _uow.BeginTransaction())
            {
                await _userRepository.SaveDtoAsync(user);
                await _emailRepository.SaveDtoAsync(email);

                var identity = GetIdentity(user);
                var token = GetToken(identity);

                var userAuth = new UserAuthDto
                {
                    UserId = user.Id,
                    LoginDateTime = DateTime.Now,
                    Token = token,
                    IpAddress = userInfo.IpAddress
                };
                await _userAuthRepository.SaveDtoAsync(userAuth);

                var res = new UserAuthInfoDto
                {
                    Token = token
                };

                return Response<UserAuthInfoDto>.GetSuccess(res);
            }
        }
    }
}