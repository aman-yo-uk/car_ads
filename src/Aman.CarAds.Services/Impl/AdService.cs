﻿using Aman.CarAds.Core.Resources;
using Aman.CarAds.Dal.Db.CarAds.Repositories;
using Aman.CarAds.Dtos.Ad;
using Aman.CarAds.Dtos.Base;
using Aman.CarAds.Dtos.User;
using Aman.CarAds.Services.Base;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Linq.Dynamic.Core;
using System.Linq;
using Aman.Tools.Extensions;
using Aman.CarAds.Dtos.Base.Extensions;
using System;
using Aman.CarAds.Dal.Db.Base;
using Aman.CarAds.Dal.Db.CarAds.Models;
using Aman.CarAds.Dtos.File;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Aman.CarAds.Dtos.ModerationHistory;

namespace Aman.CarAds.Services.Impl
{
    public class AdService : LangService, IAdService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IUow<CarAdsContext> _uow;
        private readonly IAdRepository _adRepository;
        private readonly ILocationDirRepository _locationDirRepository;
        private readonly IModelDirRepository _modelDirRepository;
        private readonly ICarBodyDirRepository _carBodyDirRepository;
        private readonly IColorDirRepository _colorDirRepository;
        private readonly IFileRepository _fileRepository;
        private readonly IAdFileRepository _adFileRepository;
        private readonly IModerationHistoryRepository _moderationHistoryRepository;

        public AdService(
            IHttpContextAccessor httpContextAccessor,
            IUow<CarAdsContext> uow,
            IAdRepository adRepository,
            ILocationDirRepository locationDirRepository,
            IModelDirRepository modelDirRepository,
            ICarBodyDirRepository carBodyDirRepository,
            IColorDirRepository colorDirRepository,
            IFileRepository fileRepository,
            IAdFileRepository adFileRepository,
            IModerationHistoryRepository moderationHistoryRepository
            ) : base(httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _uow = uow;
            _adRepository = adRepository;
            _locationDirRepository = locationDirRepository;
            _modelDirRepository = modelDirRepository;
            _carBodyDirRepository = carBodyDirRepository;
            _colorDirRepository = colorDirRepository;
            _fileRepository = fileRepository;
            _adFileRepository = adFileRepository;
            _moderationHistoryRepository = moderationHistoryRepository;
        }

        public async Task<Response<List<AdListInfoDto>>> GetListAsync(RequestAdInfosDto requestAdListInfo)
        {
            if (requestAdListInfo.PageNumber < 1)
                requestAdListInfo.PageNumber = 1;

            var query = _adRepository.GetAdFullInfos(Culture);

            if (requestAdListInfo.DynamicFilters.HasValue())
            {
                foreach (var filter in requestAdListInfo.DynamicFilters)
                {
                    query = query.Where(filter.Use());
                }
            }

            var ads = await query
                .Select(_ => new AdListInfoDto
                {
                    Id = _.Id,
                    LocationName = _.LocationName,
                    MarkName = _.MarkName,
                    ModelName = _.ModelName,
                    CarState = _.CarStateName,
                    CarBodyName = _.CarBodyName,
                    EngineType = _.EngineTypeName,
                    TransmissionType = _.TransmissionTypeName,
                    Mileage = _.Mileage,
                    Price = _.Price,
                    Text = _.Text
                })
                .Paginate(20, requestAdListInfo.PageNumber)
                .ToListAsync();

            return Response<List<AdListInfoDto>>.GetSuccess(ads);
        }

        public async Task<Response<AdInfoDto>> GetByIdAsync(long id)
        {
            var ad = await _adRepository.GetAdFullInfos(Culture)
                .Select(_ => new AdInfoDto
                {
                    Id = _.Id,
                    UserId = _.UserId,
                    State = _.State,
                    StateName = _.StateName,
                    LocationId = _.LocationId,
                    LocationName = _.LocationName,
                    MarkId = _.MarkId,
                    MarkName = _.MarkName,
                    ModelId = _.ModelId,
                    ModelName = _.ModelName,
                    VinCode = _.VinCode,
                    CarState = _.CarState,
                    CarStateName = _.CarStateName,
                    IsCustomsCleared = _.IsCustomsCleared,
                    CarBodyId = _.CarBodyId,
                    CarBodyName = _.CarBodyName,
                    EngineType = _.EngineType,
                    EngineTypeName = _.EngineTypeName,
                    TransmissionType = _.TransmissionType,
                    TransmissionTypeName = _.TransmissionTypeName,
                    SteeringWheelType = _.SteeringWheelType,
                    SteeringWheelTypeName = _.SteeringWheelTypeName,
                    DriveType = _.DriveType,
                    DriveTypeName = _.DriveTypeName,
                    Mileage = _.Mileage,
                    AvailabilityType = _.AvailabilityType,
                    AvailabilityTypeName = _.AvailabilityTypeName,
                    EngineVolume = _.EngineVolume,
                    ColorId = _.ColorId,
                    ColorName = _.ColorName,
                    IssueYear = _.IssueYear,
                    Price = _.Price,
                    Text = _.Text,
                    DeactivateReason = _.DeactivateReason
                })
                .FirstOrDefaultAsync(_ => _.Id == id);

            if (ad == null)
                return Response<AdInfoDto>.GetError(GetServiceResource("AdNotFound"));

            return Response<AdInfoDto>.GetSuccess(ad);
        }

        public async Task<Response<List<UserAdInfosDto>>> GetUserListAsync(AdStateEnum adState)
        {
            if (!IsAuthenticated)
                return Response<List<UserAdInfosDto>>.GetError(ResourceHelper.GetByCodeFromRegion("UserService", "Unauthorized", Culture));

            var ads = await _adRepository.GetAdFullInfos(Culture)
                .Where(_ => _.UserId == UserId)
                .WhereIf(adState != AdStateEnum.Undefined, _ => _.State == adState)
                .Select(_ => new UserAdInfosDto
                {
                    Id = _.Id,
                    State = _.State,
                    StateName = _.StateName,
                    LocationName = _.LocationName,
                    MarkName = _.MarkName,
                    ModelName = _.ModelName,
                    CarState = _.CarStateName,
                    CarBodyName = _.CarBodyName,
                    EngineType = _.EngineTypeName,
                    TransmissionType = _.TransmissionTypeName,
                    Mileage = _.Mileage,
                    Price = _.Price,
                    Text = _.Text
                })
                .ToListAsync();

            return Response<List<UserAdInfosDto>>.GetSuccess(ads);
        }

        public async Task<Response<bool>> AddAsync(NewAdInfoDto adInfo)
        {
            if (adInfo == null)
                return Response<bool>.GetError(GetServiceResource("NotEnoughData"));

            if (!IsAuthenticated)
                return Response<bool>.GetError(ResourceHelper.GetByCodeFromRegion("UserService", "Unauthorized", Culture));

            if (!await _locationDirRepository.GetDtoQuery().AnyAsync(_ => _.Id == adInfo.LocationId))
                return Response<bool>.GetError(GetServiceResource("UnknownPlaceOfSaleSpecified"));

            if (!await _modelDirRepository.GetDtoQuery().AnyAsync(_ => _.Id == adInfo.ModelId))
                return Response<bool>.GetError(GetServiceResource("UnknownModelSpecified"));

            if (!await _carBodyDirRepository.GetDtoQuery().AnyAsync(_ => _.Id == adInfo.CarBodyId))
                return Response<bool>.GetError(GetServiceResource("UnknownCarBodySpecified"));

            if (!await _colorDirRepository.GetDtoQuery().AnyAsync(_ => _.Id == adInfo.ColorId))
                return Response<bool>.GetError(GetServiceResource("UnknownColorSpecified"));

            if (adInfo.VinCode.IsNullOrEmpty())
                return Response<bool>.GetError(GetServiceResource("EnterVinCode"));

            var minIssueYear = 1900;
            var maxIssueYear = DateTime.Now.Year;
            if (adInfo.IssueYear < minIssueYear || adInfo.IssueYear > maxIssueYear)
                return Response<bool>.GetError(GetServiceResource("EnterCorrectIssueYear").F(minIssueYear, maxIssueYear));

            if (adInfo.Price < 1)
                return Response<bool>.GetError(GetServiceResource("EnterCorrectPrice"));

            var files = new List<FileDto>();

            if (adInfo.Photos.HasValue())
            {
                foreach (var adFile in adInfo.Photos)
                {
                    if (adFile == null)
                        continue;

                    if (adFile.DataBase64.IsNullOrEmpty())
                        continue;

                    if (adFile.MimeType.IsNullOrEmpty())
                        return Response<bool>.GetError(GetServiceResource("PhotoFileTypeNotReceived"));

                    if (adFile.MimeType.HasValue() && !adFile.MimeType.StartsWith("image/"))
                        return Response<bool>.GetError(GetServiceResource("AttachedFileNotImage"));

                    if (adFile.Name.IsNullOrEmpty())
                        return Response<bool>.GetError(GetServiceResource("PhotoFileNameNotReceived"));

                    var file = new FileDto
                    {
                        Name = adFile.Name,
                        MimeType = adFile.MimeType,
                        Data = Convert.FromBase64String(adFile.DataBase64)
                    };
                    files.Add(file);
                }
            }

            var ad = new AdDto
            {
                UserId = UserId.Value,
                State = AdStateEnum.Moderation,
                LocationId = adInfo.LocationId,
                ModelId = adInfo.ModelId,
                VinCode = adInfo.VinCode,
                CarState = adInfo.CarState,
                IsCustomsCleared = adInfo.IsCustomsCleared,
                CarBodyId = adInfo.CarBodyId,
                EngineType = adInfo.EngineType,
                TransmissionType = adInfo.TransmissionType,
                SteeringWheelType = adInfo.SteeringWheelType,
                DriveType = adInfo.DriveType,
                Mileage = adInfo.Mileage,
                AvailabilityType = adInfo.AvailabilityType,
                EngineVolume = adInfo.EngineVolume,
                ColorId = adInfo.ColorId,
                IssueYear = adInfo.IssueYear,
                Price = adInfo.Price,
                Text = adInfo.Text
            };

            using (var tran = _uow.BeginTransaction())
            {
                await _adRepository.SaveDtoAsync(ad);

                foreach (var file in files)
                {
                    await _fileRepository.SaveDtoAsync(file);

                    await _adFileRepository.SaveDtoAsync(new AdFileDto
                    {
                        AdId = ad.Id,
                        FileId = file.Id
                    });
                }

                tran.Commit();
            }

            return Response<bool>.GetSuccess(true);
        }

        public async Task<Response<bool>> EditAdAsync(EditAdInfoDto adInfo)
        {
            if (adInfo == null)
                return Response<bool>.GetError(GetServiceResource("NotEnoughData"));

            if (!IsAuthenticated)
                return Response<bool>.GetError(ResourceHelper.GetByCodeFromRegion("UserService", "Unauthorized", Culture));

            if (!await _locationDirRepository.GetDtoQuery().AnyAsync(_ => _.Id == adInfo.LocationId))
                return Response<bool>.GetError(GetServiceResource("UnknownPlaceOfSaleSpecified"));

            if (!await _modelDirRepository.GetDtoQuery().AnyAsync(_ => _.Id == adInfo.ModelId))
                return Response<bool>.GetError(GetServiceResource("UnknownModelSpecified"));

            if (!await _carBodyDirRepository.GetDtoQuery().AnyAsync(_ => _.Id == adInfo.CarBodyId))
                return Response<bool>.GetError(GetServiceResource("UnknownCarBodySpecified"));

            if (!await _colorDirRepository.GetDtoQuery().AnyAsync(_ => _.Id == adInfo.ColorId))
                return Response<bool>.GetError(GetServiceResource("UnknownColorSpecified"));

            if (adInfo.VinCode.IsNullOrEmpty())
                return Response<bool>.GetError(GetServiceResource("EnterVinCode"));

            var minIssueYear = 1900;
            var maxIssueYear = DateTime.Now.Year;
            if (adInfo.IssueYear < minIssueYear || adInfo.IssueYear > maxIssueYear)
                return Response<bool>.GetError(GetServiceResource("EnterCorrectIssueYear").F(minIssueYear, maxIssueYear));

            if (adInfo.Price < 1)
                return Response<bool>.GetError(GetServiceResource("EnterCorrectPrice"));

            var ad = await _adRepository.GetDtoByIdAsync(adInfo.Id);
            if (ad == null)
                return Response<bool>.GetError(GetServiceResource("AdNotFound"));

            ad = new AdDto
            {
                State = AdStateEnum.Moderation,
                LocationId = adInfo.LocationId,
                ModelId = adInfo.ModelId,
                VinCode = adInfo.VinCode,
                CarState = adInfo.CarState,
                IsCustomsCleared = adInfo.IsCustomsCleared,
                CarBodyId = adInfo.CarBodyId,
                EngineType = adInfo.EngineType,
                TransmissionType = adInfo.TransmissionType,
                SteeringWheelType = adInfo.SteeringWheelType,
                DriveType = adInfo.DriveType,
                Mileage = adInfo.Mileage,
                AvailabilityType = adInfo.AvailabilityType,
                EngineVolume = adInfo.EngineVolume,
                ColorId = adInfo.ColorId,
                IssueYear = adInfo.IssueYear,
                Price = adInfo.Price,
                Text = adInfo.Text
            };

            await _adRepository.SaveDtoAsync(ad);

            return Response<bool>.GetSuccess(true);
        }

        public async Task<Response<bool>> EditPhotosAsync(EditAdPhotosInfoDto adInfo)
        {
            if (adInfo == null)
                return Response<bool>.GetError(GetServiceResource("NotEnoughData"));

            var newFiles = new List<FileDto>();

            if (adInfo.Photos.HasValue())
            {
                foreach (var adFile in adInfo.Photos.Where(_ => _.Id.HasValue))
                {
                    if (adFile == null)
                        continue;

                    if (adFile.DataBase64.IsNullOrEmpty())
                        continue;

                    if (adFile.MimeType.IsNullOrEmpty())
                        return Response<bool>.GetError(GetServiceResource("PhotoFileTypeNotReceived"));

                    if (adFile.MimeType.HasValue() && !adFile.MimeType.StartsWith("image/"))
                        return Response<bool>.GetError(GetServiceResource("AttachedFileNotImage"));

                    if (adFile.Name.IsNullOrEmpty())
                        return Response<bool>.GetError(GetServiceResource("PhotoFileNameNotReceived"));

                    var file = new FileDto
                    {
                        Name = adFile.Name,
                        MimeType = adFile.MimeType,
                        Data = Convert.FromBase64String(adFile.DataBase64)
                    };
                    newFiles.Add(file);
                }
            }

            var ad = await _adRepository.GetDtoByIdAsync(adInfo.Id);
            if (ad == null)
                return Response<bool>.GetError(GetServiceResource("AdNotFound"));

            var oldPhotoIds = adInfo.Photos.Where(_ => _.Id.HasValue).Select(_ => _.Id).ToList();
            var adPhotosForDelete = await _adFileRepository.GetDtoQuery()
                .Where(_ => _.AdId == adInfo.Id && !oldPhotoIds.Contains(_.FileId))
                .ToListAsync();
            var fileForDeleteIds = adPhotosForDelete.Select(_ => _.FileId).ToList();
            var photosForDelete = await _fileRepository.GetDtoQuery()
                .Where(_ => fileForDeleteIds.Contains(_.Id))
                .ToListAsync();

            using (var tran = _uow.BeginTransaction())
            {
                foreach (var file in newFiles)
                {
                    await _fileRepository.SaveDtoAsync(file);

                    await _adFileRepository.SaveDtoAsync(new AdFileDto
                    {
                        AdId = ad.Id,
                        FileId = file.Id
                    });
                }

                foreach (var adPhotoForDelete in adPhotosForDelete)
                {
                    await _adFileRepository.DeleteDtoAsync(adPhotoForDelete);
                }

                foreach (var photoForDelete in photosForDelete)
                {
                    await _fileRepository.DeleteDtoAsync(photoForDelete);
                }

                tran.Commit();
            }

            return Response<bool>.GetSuccess(true);
        }

        public async Task<Response<bool>> DeleteAsync(long id)
        {
            if (!IsAuthenticated)
                return Response<bool>.GetError(ResourceHelper.GetByCodeFromRegion("UserService", "Unauthorized", Culture));

            var ad = await _adRepository.GetDtoByIdAsync(id);

            if (ad.UserId != UserId)
                return Response<bool>.GetError(GetServiceResource("NoAccess"));

            if (ad == null)
                return Response<bool>.GetError(GetServiceResource("AdNotFound"));

            await _adRepository.DeleteDtoAsync(ad);

            return Response<bool>.GetSuccess(true);
        }

        public async Task<Response<bool>> ArchivateAsync(long id)
        {
            if (!IsAuthenticated)
                return Response<bool>.GetError(ResourceHelper.GetByCodeFromRegion("UserService", "Unauthorized", Culture));

            var ad = await _adRepository.GetDtoByIdAsync(id);

            if (ad.UserId != UserId)
                return Response<bool>.GetError(GetServiceResource("NoAccess"));

            if (ad == null)
                return Response<bool>.GetError(GetServiceResource("AdNotFound"));

            ad.State = AdStateEnum.Archivated;
            await _adRepository.SaveDtoAsync(ad);

            return Response<bool>.GetSuccess(true);
        }

        public async Task<Response<bool>> ActivateAsync(long id)
        {
            if (!IsAuthenticated)
                return Response<bool>.GetError(ResourceHelper.GetByCodeFromRegion("UserService", "Unauthorized", Culture));

            if (UserType != UserTypeEnum.Moderator && UserType != UserTypeEnum.Admin)
                return Response<bool>.GetError(GetServiceResource("NoAccess"));

            var ad = await _adRepository.GetDtoByIdAsync(id);

            if (ad == null)
                return Response<bool>.GetError(GetServiceResource("AdNotFound"));

            var moderationHistory = new ModerationHistoryDto
            {
                AdId = id,
                UserId = UserId.Value,
                DecisionType = DecisionTypeEnum.Approved
            };

            ad.State = AdStateEnum.Published;

            using (var tran = _uow.BeginTransaction())
            {
                await _moderationHistoryRepository.SaveDtoAsync(moderationHistory);

                await _adRepository.SaveDtoAsync(ad);

                tran.Commit();
            }

            return Response<bool>.GetSuccess(true);
        }

        public async Task<Response<bool>> DeactivateAsync(AdDeactivateInfoDto adDeactivateInfo)
        {
            if (!IsAuthenticated)
                return Response<bool>.GetError(ResourceHelper.GetByCodeFromRegion("UserService", "Unauthorized", Culture));

            if (UserType != UserTypeEnum.Moderator && UserType != UserTypeEnum.Admin)
                return Response<bool>.GetError(GetServiceResource("NoAccess"));

            var ad = await _adRepository.GetDtoByIdAsync(adDeactivateInfo.Id);

            if (ad == null)
                return Response<bool>.GetError(GetServiceResource("AdNotFound"));

            var moderationHistory = new ModerationHistoryDto
            {
                AdId = adDeactivateInfo.Id,
                UserId = UserId.Value,
                DecisionType = DecisionTypeEnum.Approved,
                RejectReason = adDeactivateInfo.Reason
            };

            ad.State = AdStateEnum.Deactivated;
            ad.DeactivateReason = adDeactivateInfo.Reason;

            using (var tran = _uow.BeginTransaction())
            {
                await _moderationHistoryRepository.SaveDtoAsync(moderationHistory);

                await _adRepository.SaveDtoAsync(ad);

                tran.Commit();
            }

            return Response<bool>.GetSuccess(true);
        }
    }
}