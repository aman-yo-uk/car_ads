﻿using System;
using System.Linq;

namespace Aman.Tools.Extensions
{
    public static class StringExtensions
    {
        public static bool HasValue(this string @string)
        {
            return !string.IsNullOrEmpty(@string);
        }

        public static bool HasValue(this string @string, string value)
        {
            return !string.IsNullOrEmpty(@string) && @string == value;
        }

        public static bool IsNullOrEmpty(this string @string)
        {
            return string.IsNullOrEmpty(@string);
        }

        public static bool IsNullOrWhiteSpace(this string @string)
        {
            return string.IsNullOrWhiteSpace(@string);
        }

        public static string Clear(this string @string, string chars)
        {
            string result = @string;
            foreach (var ch in chars)
            {
                result = result.Replace(ch.ToString(), "");
            }
            return result;
        }

        public static bool StartsWith(this string @string, params string[] strings)
        {
            foreach (var str in strings)
            {
                if (@string.StartsWith(str))
                    return true;
            }
            return false;
        }

        public static bool ContainsOnly(this string @string, char @char)
        {
            return @string.All(s => s == @char);
        }

        public static string F(this string @string, params object[] values)
        {
            return @string.HasValue() ? string.Format(@string, values) : null;
        }

        public static string ToLowerNullable(this string @string)
        {
            return @string.HasValue() ? @string.ToLower() : @string;
        }
        public static string ToUpperNullable(this string @string)
        {
            return @string.HasValue() ? @string.ToUpper() : @string;
        }
        public static bool In(this string @string, params string[] values)
        {
            return values.Contains(@string);
        }
        public static string TrimEndLine(this string @string)
        {
            if (@string.IsNullOrEmpty())
                return @string;
            return @string.TrimEnd('\r', '\n');
        }
        public static string TrimNullable(this string @string, params char[] chars)
        {
            if (@string.IsNullOrEmpty())
                return @string;
            return @string.Trim(chars);
        }
        public static string AppendLine(this string @string, string addString)
        {
            if (addString.IsNullOrEmpty())
                return @string;
            if (@string.IsNullOrEmpty())
                return addString;
            return $"{@string}{Environment.NewLine}{addString}";
        }

        public static bool HasOnlyDigits(this string @string)
        {
            foreach (char c in @string)
            {
                if (c < '0' || c > '9')
                    return false;
            }
            return true;
        }
    }
}