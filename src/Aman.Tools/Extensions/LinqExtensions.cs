﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Aman.Tools.Extensions
{
    public static class LinqExtensions
    {
        public static IQueryable<TSource> WhereIf<TSource>(this IQueryable<TSource> source, bool condition, Expression<Func<TSource, bool>> predicate)
        {
            if (condition)
            {
                return source.Where(predicate);
            }
            return source;
        }

        public static IQueryable<TSource> Paginate<TSource>(this IQueryable<TSource> source, int pageSize, int pageNumber, int? rowCount = null)
        {
            if (rowCount == null)
            {
                rowCount = source.Count();
            }
            var calcPageSize = pageSize <= 1 ? 1 : pageSize;
            var calcPageNumber = pageNumber < 1
                ? 1
                : calcPageSize * pageNumber > rowCount
                    ? rowCount % calcPageSize > 0
                        ? (int)(rowCount / calcPageSize) + 1
                        : (int)(rowCount / calcPageSize)
                    : pageNumber;
            var skipCount = calcPageSize * (calcPageNumber - 1);

            return source.Skip(skipCount > 0 ? skipCount : 0).Take(calcPageSize);
        }
    }
}