﻿using System.Collections;

namespace Aman.Tools.Extensions
{
    public static class ListExtensions
    {
        public static bool IsNullOrEmpty(this IList list)
        {
            return list == null || list.Count == 0;
        }
        public static bool HasValue(this IList list)
        {
            return !list.IsNullOrEmpty();
        }
    }
}