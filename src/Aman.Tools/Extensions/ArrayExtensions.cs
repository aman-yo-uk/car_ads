﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;

namespace Aman.Tools.Extensions
{
    public static class ArrayExtensions
    {
        public static bool IsNullOrEmpty(this Array @array)
        {
            return @array == null || array.Length == 0;
        }
        public static bool HasValue(this Array @array)
        {
            return !@array.IsNullOrEmpty();
        }

        public static bool Equal(this IEnumerable array1, IEnumerable array2)
        {
            if (array1 == null && array2 == null) return true;
            if (array1 == null || array2 == null) return false;

            if (array1.ToDynamicArray().Count() != array2.ToDynamicArray().Count()) return false;
            foreach (var item1 in array1)
            {
                var hasEqual = false;
                foreach (var item2 in array2)
                {
                    if (item1.Equals(item2))
                    {
                        hasEqual = true;
                        break;
                    }
                }
                if (!hasEqual) return false;
            }
            return true;
        }

        public static bool Equal<T>(this IEnumerable<T> array1, IEnumerable<T> array2, Func<T, object> selectMember)
        {
            if (array1 == null && array2 == null)
                return true;
            if (array1 == null || array2 == null)
                return false;
            var selArray1 = array1.Select(selectMember);
            var selArray2 = array2.Select(selectMember);
            return selArray1.Equal(selArray2);
        }
    }
}