﻿using System;
using System.IO;
using System.Linq;
using System.Threading;

namespace Aman.Tools.Helpers
{
    public static class FileHelper
    {
        public static void AppendAllText(string path, string content)
        {
            var directory = Path.GetDirectoryName(path);
            CreateDirIfNeed(directory);
            File.AppendAllText(path, content);
        }

        public static void WriteAllText(string path, string content)
        {
            var directory = Path.GetDirectoryName(path);
            CreateDirIfNeed(directory);
            File.WriteAllText(path, content);
        }

        public static string ReadContent(string path)
        {
            Exception lastEx = null;
            for (int i = 0; i < 10; i++)
            {
                try
                {
                    if (File.Exists(path))
                    {
                        return File.ReadAllText(path);
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    lastEx = ex;
                }
                Thread.Sleep(1000);
            }
            throw lastEx;
        }

        public static void DeleteDirectory(string path)
        {
            Exception lastEx = null;
            for (int i = 0; i < 10; i++)
            {
                try
                {
                    Directory.Delete(path, true);
                    return;
                }
                catch (Exception ex)
                {
                    lastEx = ex;
                }
                Thread.Sleep(1000);
            }
            throw lastEx;
        }

        public static void SaveIfNeed(byte[] data, string path)
        {
            if (File.Exists(path))
            {
                var existFileData = File.ReadAllBytes(path);
                if (existFileData.SequenceEqual(data))
                    return;
            }

            //Проверяем наличие каталога
            var dirPath = Path.GetDirectoryName(path);
            CreateDirIfNeed(dirPath);

            Stream dataStream = new MemoryStream(data);
            using (Stream fileStream = File.OpenWrite(path))
            {
                byte[] buffer = new byte[8 * 1024];
                int len;
                while ((len = dataStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    fileStream.Write(buffer, 0, len);
                }
            }
        }

        public static void CreateDirIfNeed(string path)
        {
            if (path != null && !Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        public static void DeleteFile(string path)
        {
            try
            {
                if (File.Exists(path))
                    File.Delete(path);
            }
            catch (Exception e)
            {
                // ignored
            }
        }
    }
}