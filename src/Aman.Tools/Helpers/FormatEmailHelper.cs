﻿using Aman.Tools.Dtos.Helpers.FormatEmailHelper;
using Aman.Tools.Extensions;
using System;
using System.Text.RegularExpressions;

namespace Aman.Tools.Helpers
{
    public static class FormatEmailHelper
    {
        public static string[] FormatEmailsString(string emails)
        {
            var emailsString = emails
                .Replace(",", "|")
                .Replace(";", "|");
            return emailsString.Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
        }

        public static FormatEmailResultDto FormatEmail(string email)
        {
            if (email.IsNullOrEmpty())
            {
                return new FormatEmailResultDto
                {
                    IsSuccess = false,
                    Email = email,
                    FormatEmailError = FormatEmailErrorEnum.EmailIsEmpty
                };
            }

            var clearEmail = StringExtensions.Clear(email.Trim(), @" ");

            var emailIsMatch = Regex.IsMatch(clearEmail,
                @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                RegexOptions.IgnoreCase);

            if (!emailIsMatch)
            {
                return new FormatEmailResultDto
                {
                    IsSuccess = false,
                    Email = email,
                    FormatEmailError = FormatEmailErrorEnum.FormatIncorrect
                };
            }

            return new FormatEmailResultDto
            {
                IsSuccess = true,
                Email = clearEmail
            };
        }
    }
}