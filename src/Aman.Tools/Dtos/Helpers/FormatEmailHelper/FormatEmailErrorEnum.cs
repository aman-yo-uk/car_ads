﻿namespace Aman.Tools.Dtos.Helpers.FormatEmailHelper
{
    public enum FormatEmailErrorEnum
    {
        Undefined = 0,
        EmailIsEmpty = 1,
        FormatIncorrect = 2
    }
}