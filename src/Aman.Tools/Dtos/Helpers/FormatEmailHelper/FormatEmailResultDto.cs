﻿namespace Aman.Tools.Dtos.Helpers.FormatEmailHelper
{
    public class FormatEmailResultDto
    {
        public bool IsSuccess { get; set; }
        public string Email { get; set; }
        public FormatEmailErrorEnum? FormatEmailError { get; set; }
    }
}