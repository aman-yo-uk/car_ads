using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Aman.CarAds.Core.Resources;
using Aman.CarAds.Dal.Db.Base;
using Aman.CarAds.Dal.Db.Base.Impl;
using Aman.CarAds.Dal.Db.CarAds.Models;
using Aman.CarAds.Dal.Db.CarAds.Repositories;
using Aman.CarAds.Dtos.Base;
using Aman.CarAds.Services;
using Aman.CarAds.Services.Base;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Aman.CarAds.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IUow<CarAdsContext>, Uow<CarAdsContext>>();

            var assemblyServices = Assembly.GetAssembly(typeof(IUserService));
            var serviceTypes = assemblyServices.GetTypes().Where(t => !t.IsInterface).Where(t => t.Name.EndsWith("Service"));
            AddServiceDescriptor(serviceTypes, services, assemblyServices.FullName);

            var assemblyRepositories = Assembly.GetAssembly(typeof(IUserRepository));
            var repositoryTypes = assemblyRepositories.GetTypes().Where(t => !t.IsInterface).Where(t => t.Name.EndsWith("Repository"));
            AddServiceDescriptor(repositoryTypes, services, assemblyRepositories.FullName);

            services
                .AddControllers()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                });

            services.AddHttpContextAccessor();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = true;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = AuthService.ValidateIssuer,
                        ValidIssuer = AuthService.Issuer,
                        ValidateAudience = AuthService.ValidateAudience,
                        ValidAudience = AuthService.Audience,
                        ValidateLifetime = AuthService.ValidateLifetime,
                        ValidateIssuerSigningKey = AuthService.ValidateIssuerSigningKey,
                        IssuerSigningKey = AuthService.GetSymmetricSecurityKey()
                    };
                });

            services.AddSwaggerGen(_ =>
            {
                _.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "JWT Authorization header using the Bearer scheme.\r\n\r\n" +
                    "Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\n" +
                    "Example: \"Bearer 12345abcdef\"",
                });
                _.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] {}
                    }
                });
            });
        }

        public void AddServiceDescriptor(IEnumerable<Type> types, IServiceCollection services, string assemblyFullName)
        {
            foreach (var type in types)
            {
                var iface = type.GetInterface("I" + type.Name);

                if (iface != null && iface.Assembly.FullName == assemblyFullName)
                {
                    var existing = services.SingleOrDefault(s => s.ServiceType.FullName == iface.FullName);
                    if (existing != null)
                        services.Remove(existing);

                    services.Add(new ServiceDescriptor(iface, type, ServiceLifetime.Scoped));
                }
            }
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            ResourceHelper.Init(ResourceNotFoundBehaviorEnum.ReturnResourceKey);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler(appError =>
                {
                    appError.Run(async context =>
                    {
                        context.Response.ContentType = "application/json";

                        var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                        if (contextFeature != null)
                        {
                            // TODO log exception

                            await context.Response.WriteAsync(JsonConvert.SerializeObject(Response.GetError(contextFeature.Error.Message), new JsonSerializerSettings()), Encoding.UTF8);
                        }
                    });
                });

                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(_ =>
            {
                _.SwaggerEndpoint("/swagger/v1/swagger.json", "Aman's CarAds API");
            });
        }
    }
}