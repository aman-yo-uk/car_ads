﻿using Aman.CarAds.Dtos.Base;
using Aman.CarAds.Dtos.User;
using Aman.CarAds.Dtos.UserAuth;
using Aman.CarAds.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Aman.CarAds.Api.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    public class AuthController : BaseController
    {
        private readonly IUserAuthService _authService;

        public AuthController(
            IUserAuthService authService
            )
        {
            _authService = authService;
        }

        [HttpPost]
        [Route("login")]
        public async Task<Response<UserAuthInfoDto>> Login(LoginUserInfoDto loginUserInfo)
        {
            return await _authService.LoginAsync(loginUserInfo);
        }

        [HttpPost]
        [Route("register")]
        public async Task<Response<UserAuthInfoDto>> Register(RegisterUserInfoDto userInfo)
        {
            return await _authService.RegisterAsync(userInfo);
        }
    }
}