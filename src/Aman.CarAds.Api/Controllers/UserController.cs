﻿using Aman.CarAds.Dtos.Base;
using Aman.CarAds.Dtos.User;
using Aman.CarAds.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Aman.CarAds.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class UserController : BaseController
    {
        private readonly IUserService _userService;

        public UserController(
            IUserService userService
            )
        {
            _userService = userService;
        }

        [HttpGet]
        [Route("profile")]
        public async Task<Response<UserProfileDto>> GetUserProfile()
        {
            return await _userService.GetProfileAsync();
        }

        [HttpPost]
        [Route("add/moderator")]
        public async Task<Response<bool>> AddModerator(ModeratorInfoDto moderatorInfo)
        {
            return await _userService.AddModeratorAsync(moderatorInfo);
        }

        [HttpPost]
        [Route("edit")]
        public async Task<Response<bool>> EditUser(UserInfoDto userInfo)
        {
            return await _userService.EditUserAsync(userInfo);
        }

        [HttpPost]
        [Route("change/secureKey")]
        public async Task<Response<bool>> ChangeSecureKey(ChangeSecureKeyDto changeSecureKey)
        {
            return await _userService.ChangeSecureKeyAsync(changeSecureKey);
        }

        [HttpPost]
        [Route("ban")]
        public async Task<Response<bool>> BanUser(BanUserDto banUserInfo)
        {
            return await _userService.BanUserAsync(banUserInfo);
        }

        [HttpPost]
        [Route("del")]
        public async Task<Response<bool>> DeleteUser(long id)
        {
            return await _userService.DeleteAsync(id);
        }
    }
}