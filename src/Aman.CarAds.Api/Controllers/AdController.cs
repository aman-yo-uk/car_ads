﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Aman.CarAds.Dtos.Ad;
using Aman.CarAds.Dtos.Base;
using Aman.CarAds.Dtos.File;
using Aman.CarAds.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Aman.CarAds.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class AdController : BaseController
    {
        private readonly IAdService _adService;

        public AdController(
            IAdService adService
            )
        {
            _adService = adService;
        }

        [HttpPost]
        [Route("")]
        [AllowAnonymous]
        public async Task<Response<List<AdListInfoDto>>> Get(RequestAdInfosDto requestAdListInfo)
        {
            return await _adService.GetListAsync(requestAdListInfo);
        }

        [HttpGet]
        [Route("{id}")]
        [AllowAnonymous]
        public async Task<Response<AdInfoDto>> Get(long id)
        {
            return await _adService.GetByIdAsync(id);
        }

        [HttpGet]
        [Route("user/list")]
        public async Task<Response<List<UserAdInfosDto>>> GetUserAds(AdStateEnum adState = AdStateEnum.Undefined)
        {
            return await _adService.GetUserListAsync(adState);
        }

        [HttpPost]
        [Route("add")]
        public async Task<Response<bool>> Add(NewAdInfoDto adInfo)
        {
            return await _adService.AddAsync(adInfo);
        }

        [HttpPost]
        [Route("edit")]
        public async Task<Response<bool>> Edit(EditAdInfoDto adInfo)
        {
            return await _adService.EditAdAsync(adInfo);
        }

        [HttpPost]
        [Route("edit/photos")]
        public async Task<Response<bool>> EditPhotos(EditAdPhotosInfoDto adInfo)
        {
            return await _adService.EditPhotosAsync(adInfo);
        }

        [HttpPost]
        [Route("del")]
        public async Task<Response<bool>> Delete(long id)
        {
            return await _adService.DeleteAsync(id);
        }

        [HttpPost]
        [Route("archivate")]
        public async Task<Response<bool>> Archivate(long id)
        {
            return await _adService.ArchivateAsync(id);
        }

        [HttpPost]
        [Route("activate")]
        public async Task<Response<bool>> Activate(long id)
        {
            return await _adService.ActivateAsync(id);
        }

        [HttpPost]
        [Route("deactivate")]
        public async Task<Response<bool>> Deactivate(AdDeactivateInfoDto adDeactivateInfo)
        {
            return await _adService.DeactivateAsync(adDeactivateInfo);
        }
    }
}