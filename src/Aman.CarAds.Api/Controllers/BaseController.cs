﻿using Microsoft.AspNetCore.Mvc;

namespace Aman.CarAds.Api.Controllers
{
    [ApiController]
    [Produces("application/json")]
    public class BaseController : ControllerBase
    {
    }
}