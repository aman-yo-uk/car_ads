﻿using System;

namespace Aman.CarAds.Dtos.User
{
    public class UserInfoDto
    {
        public string Nickname { get; set; }
        public DateTime? BirthDate { get; set; }
        public GenderTypeEnum GenderType { get; set; }
    }
}