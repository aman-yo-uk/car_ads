﻿namespace Aman.CarAds.Dtos.User
{
    public class ModeratorInfoDto
    {
        public string Login { get; set; }
        public string SecureKey { get; set; }
    }
}