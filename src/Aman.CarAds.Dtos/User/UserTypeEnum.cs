﻿namespace Aman.CarAds.Dtos.User
{
    public enum UserTypeEnum
    {
        Undefined = 0,
        System = 1,
        Admin = 2,
        Moderator = 3,
        User = 4
    }
}