﻿namespace Aman.CarAds.Dtos.User
{
    public enum GenderTypeEnum
    {
        Undefined = 0,
        Male = 1,
        Female = 2
    }
}