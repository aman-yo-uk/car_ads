﻿using System;

namespace Aman.CarAds.Dtos.User
{
    public class UserProfileDto
    {
        public string Login { get; set; }
        public string Nickname { get; set; }
        public DateTime? BirthDate { get; set; }
        public GenderTypeEnum GenderType { get; set; }
    }
}