﻿namespace Aman.CarAds.Dtos.User
{
    public class ChangeSecureKeyDto
    {
        public string SecureKey { get; set; }
        public string SecureKeyRepeat { get; set; }
    }
}