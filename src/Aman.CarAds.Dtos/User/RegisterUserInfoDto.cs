﻿namespace Aman.CarAds.Dtos.User
{
    public class RegisterUserInfoDto
    {
        public string Login { get; set; }
        public string SecureKey { get; set; }
        public string SecureKeyRepeat { get; set; }
        public string IpAddress { get; set; }
    }
}