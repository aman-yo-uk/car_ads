﻿using Aman.CarAds.Dtos.Base.Impl;
using System;

namespace Aman.CarAds.Dtos.User
{
    public class UserDto : BaseDto
    {
        public string Login { get; set; }
        public string SecureKeyHash { get; set; }
        public UserTypeEnum Type { get; set; }
        public string Nickname { get; set; }
        public DateTime? BirthDate { get; set; }
        public GenderTypeEnum GenderType { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime? BannedTo { get; set; }
        public string BannedReason { get; set; }
    }
}