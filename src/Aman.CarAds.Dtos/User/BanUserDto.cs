﻿using System;

namespace Aman.CarAds.Dtos.User
{
    public class BanUserDto
    {
        public long UserId { get; set; }
        public DateTime BannedTo { get; set; }
        public string BannedReason { get; set; }
    }
}