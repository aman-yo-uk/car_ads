﻿using Aman.CarAds.Dtos.Base.Impl;
using Aman.CarAds.Dtos.User;
using System;

namespace Aman.CarAds.Dtos.UserAuth
{
    public class UserAuthDto : BaseDto
    {
        public long UserId { get; set; }
        public UserDto User { get; set; }
        public DateTime LoginDateTime { get; set; }
        public string Token { get; set; }
        public string IpAddress { get; set; }
    }
}