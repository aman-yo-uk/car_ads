﻿namespace Aman.CarAds.Dtos.UserAuth
{
    public class UserAuthInfoDto
    {
        public string Token { get; set; }
    }
}