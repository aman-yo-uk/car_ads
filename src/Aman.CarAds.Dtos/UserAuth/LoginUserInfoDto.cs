﻿namespace Aman.CarAds.Dtos.UserAuth
{
    public class LoginUserInfoDto
    {
        public string Login { get; set; }
        public string SecureKey { get; set; }
        public string IpAddress { get; set; }
    }
}