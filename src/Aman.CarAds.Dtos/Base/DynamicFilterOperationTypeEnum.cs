﻿namespace Aman.CarAds.Dtos.Base
{
    public enum DynamicFilterOperationTypeEnum
    {
        Undefined = 0,
        Equal = 1,
        More = 2,
        MoreEqual = 3,
        Less = 4,
        LessEqual = 5,
        Contains = 6,
        StartsWith = 7,
        EndsWith = 8
    }
}