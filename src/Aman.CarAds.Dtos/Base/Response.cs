﻿using Newtonsoft.Json;

namespace Aman.CarAds.Dtos.Base
{
    /// <summary>
    /// Базовый ответ не типизированный (void)
    /// </summary>
    public class Response
    {
        /// <summary>
        /// Признак успешной обработки запроса
        /// </summary>
        public bool IsSuccess { get; protected set; }

        /// <summary>
        /// Тип ошибки
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public ResponseErrorTypeEnum ErrorType { get; protected set; }

        /// <summary>
        /// Код ошибки
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string ErrorCode { get; protected set; }

        /// <summary>
        /// Текст ошибки
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string ErrorMessage { get; protected set; }

        public static Response GetSuccess()
        {
            return new Response
            {
                IsSuccess = true,
            };
        }

        public static Response GetError(string errorMessage)
        {
            return new Response
            {
                IsSuccess = false,
                ErrorMessage = errorMessage
            };
        }
    }
}