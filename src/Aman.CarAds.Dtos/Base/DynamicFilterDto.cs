﻿namespace Aman.CarAds.Dtos.Base
{
    public class DynamicFilterDto
    {
        public string Field { get; set; }
        public string Value { get; set; }
        public DynamicFilterOperationTypeEnum OperationType { get; set; }
    }
}