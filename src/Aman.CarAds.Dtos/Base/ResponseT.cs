﻿using System;

namespace Aman.CarAds.Dtos.Base
{
    /// <summary>
    /// Базовый ответ типизированный
    /// </summary>
    public class Response<T> : Response
    {
        /// <summary>
        /// Ответ метода сервиса
        /// </summary>
        public T Result { get; set; }

        public static Response<T> GetSuccess(T result)
        {
            return new Response<T>
            {
                IsSuccess = true,
                ErrorType = ResponseErrorTypeEnum.Undefined,
                ErrorCode = null,
                Result = result
            };
        }

        public static Response<T> GetError(ResponseErrorTypeEnum errorType, string errorCode = null, string errorMessage = null, T result = default(T))
        {
            return new Response<T>
            {
                IsSuccess = false,
                ErrorType = errorType,
                ErrorCode = errorCode,
                ErrorMessage = errorMessage,
                Result = result
            };
        }

        public static Response<T> GetError(Exception ex = null, T result = default(T), bool logError = true)
        {
            // TODO log exception
            //if (logError)
            //    LogManager.GetCurrentClassLogger().Error(ex);
            return new Response<T>
            {
                IsSuccess = false,
                ErrorType = ResponseErrorTypeEnum.System,
                ErrorMessage = ex.ToString(),
                Result = result
            };
        }

        public static Response<T> GetError(string errorMessage)
        {
            return new Response<T>
            {
                IsSuccess = false,
                ErrorMessage = errorMessage
            };
        }
    }
}