﻿using Newtonsoft.Json;

namespace Aman.CarAds.Dtos.Base.Impl
{
    public class BaseDto : IDto
    {
        public long Id { get; set; }

        [JsonIgnore]
        public bool IsDeleted { get; set; }
    }
}