﻿namespace Aman.CarAds.Dtos.Base
{
    /// <summary>
    /// Базовый ответ не типизированный (void)
    /// </summary>
    public enum ResponseErrorTypeEnum
    {
        Undefined = 0,
        System = 1,
        BusinessLogic = 2
    }
}