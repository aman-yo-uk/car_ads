﻿namespace Aman.CarAds.Dtos.Base.Extensions
{
    public static class DynamicFilterExtensions
    {
        public static string Use(this DynamicFilterDto dynamicFilter)
        {
            return dynamicFilter.OperationType switch
            {
                DynamicFilterOperationTypeEnum.Equal => $"{dynamicFilter.Field} == {dynamicFilter.Value}",
                DynamicFilterOperationTypeEnum.More => $"{dynamicFilter.Field} > {dynamicFilter.Value}",
                DynamicFilterOperationTypeEnum.MoreEqual => $"{dynamicFilter.Field} >= {dynamicFilter.Value}",
                DynamicFilterOperationTypeEnum.Less => $"{dynamicFilter.Field} < {dynamicFilter.Value}",
                DynamicFilterOperationTypeEnum.LessEqual => $"{dynamicFilter.Field} <= {dynamicFilter.Value}",
                DynamicFilterOperationTypeEnum.Contains => $"{dynamicFilter.Field}.Contains({dynamicFilter.Value})",
                DynamicFilterOperationTypeEnum.StartsWith => $"{dynamicFilter.Field}.StartsWith({dynamicFilter.Value})",
                DynamicFilterOperationTypeEnum.EndsWith => $"{dynamicFilter.Field}.EndsWith({dynamicFilter.Value})",
                _ => "",
            };
        }
    }
}