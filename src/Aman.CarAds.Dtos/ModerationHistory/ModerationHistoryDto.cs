﻿using Aman.CarAds.Dtos.Ad;
using Aman.CarAds.Dtos.Base.Impl;
using Aman.CarAds.Dtos.User;

namespace Aman.CarAds.Dtos.ModerationHistory
{
    public class ModerationHistoryDto : BaseDto
    {
        public long UserId { get; set; }
        public UserDto User { get; set; }
        public long AdId { get; set; }
        public AdDto Ad { get; set; }
        public DecisionTypeEnum DecisionType { get; set; }
        public string RejectReason { get; set; }
    }
}