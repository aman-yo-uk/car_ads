﻿namespace Aman.CarAds.Dtos.ModerationHistory
{
    public enum DecisionTypeEnum
    {
        Undefined = 0,
        /// <summary>
        /// Одобрено
        /// </summary>
        Approved = 1,
        /// <summary>
        /// Отказано
        /// </summary>
        Rejected = 2
    }
}