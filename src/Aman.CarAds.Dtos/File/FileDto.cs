﻿using Aman.CarAds.Dtos.Base.Impl;

namespace Aman.CarAds.Dtos.File
{
    public class FileDto : BaseDto
    {
        public string Name { get; set; }
        public string MimeType { get; set; }
        public byte[] Data { get; set; }
    }
}