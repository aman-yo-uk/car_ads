﻿namespace Aman.CarAds.Dtos.File
{
    public class EditAdPhotoDto
    {
        public long? Id { get; set; }
        public string Name { get; set; }
        public string MimeType { get; set; }
        public string DataBase64 { get; set; }
    }
}