﻿namespace Aman.CarAds.Dtos.File
{
    public class FileInfoDto
    {
        public string Name { get; set; }
        public string MimeType { get; set; }
        public string DataBase64 { get; set; }
    }
}