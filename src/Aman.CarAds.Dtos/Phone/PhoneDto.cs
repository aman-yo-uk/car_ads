﻿using Aman.CarAds.Dtos.Base.Impl;
using Aman.CarAds.Dtos.User;

namespace Aman.CarAds.Dtos.Phone
{
    public class PhoneDto : BaseDto
    {
        public long UserId { get; set; }
        public UserDto User { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsConfirmed { get; set; }
    }
}