﻿namespace Aman.CarAds.Dtos.Ad
{
    public enum AvailabilityTypeEnum
    {
        Undefined = 0,
        /// <summary>
        /// в наличии
        /// </summary>
        InStock = 1,
        /// <summary>
        /// на заказ
        /// </summary>
        ToOrder = 2
    }
}