﻿namespace Aman.CarAds.Dtos.Ad
{
    public enum AdSortTypeEnum
    {
        Undefined = 0,
        /// <summary>
        /// По дате публикации
        /// </summary>
        ByDate = 1,
        /// <summary>
        /// По цене
        /// </summary>
        ByPrice = 2
    }
}