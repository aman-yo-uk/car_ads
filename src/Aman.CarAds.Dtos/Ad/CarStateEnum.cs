﻿namespace Aman.CarAds.Dtos.Ad
{
    public enum CarStateEnum
    {
        Undefined = 0,
        /// <summary>
        /// новая
        /// </summary>
        New = 1,
        /// <summary>
        /// с пробегом
        /// </summary>
        WithMileage = 2
    }
}