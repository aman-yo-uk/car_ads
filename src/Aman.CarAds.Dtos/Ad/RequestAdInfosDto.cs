﻿using Aman.CarAds.Dtos.Base;
using System.Collections.Generic;

namespace Aman.CarAds.Dtos.Ad
{
    public class RequestAdInfosDto
    {
        public int PageNumber { get; set; }
        public List<DynamicFilterDto> DynamicFilters { get; set; }
        public AdSortTypeEnum SortType { get; set; }
    }
}