﻿using Aman.CarAds.Dtos.File;
using System.Collections.Generic;

namespace Aman.CarAds.Dtos.Ad
{
    public class EditAdPhotosInfoDto
    {
        public long Id { get; set; }
        public List<EditAdPhotoDto> Photos { get; set; }
    }
}