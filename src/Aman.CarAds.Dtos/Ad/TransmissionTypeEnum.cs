﻿namespace Aman.CarAds.Dtos.Ad
{
    public enum TransmissionTypeEnum
    {
        Undefined = 0,
        /// <summary>
        /// механика
        /// </summary>
        Manual = 1,
        /// <summary>
        /// автомат
        /// </summary>
        Automatic = 2,
        /// <summary>
        /// типтроник
        /// </summary>
        Tiptronic = 3,
        /// <summary>
        /// вариатор
        /// </summary>
        Variator = 4,
        /// <summary>
        /// робот
        /// </summary>
        Robot = 5
    }
}