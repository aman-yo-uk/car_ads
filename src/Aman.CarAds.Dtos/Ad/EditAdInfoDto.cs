﻿using Aman.CarAds.Dtos.File;
using System.Collections.Generic;

namespace Aman.CarAds.Dtos.Ad
{
    public class EditAdInfoDto
    {
        public long Id { get; set; }
        public long LocationId { get; set; }
        public long ModelId { get; set; }
        public string VinCode { get; set; }
        public CarStateEnum CarState { get; set; }
        public bool IsCustomsCleared { get; set; }
        public long CarBodyId { get; set; }
        public EngineTypeEnum EngineType { get; set; }
        public TransmissionTypeEnum TransmissionType { get; set; }
        public SteeringWheelTypeEnum SteeringWheelType { get; set; }
        public DriveTypeEnum DriveType { get; set; }
        public int Mileage { get; set; }
        public AvailabilityTypeEnum AvailabilityType { get; set; }
        public decimal EngineVolume { get; set; }
        public long ColorId { get; set; }
        public int IssueYear { get; set; }
        public int Price { get; set; }
        public string Text { get; set; }
    }
}