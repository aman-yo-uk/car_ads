﻿namespace Aman.CarAds.Dtos.Ad
{
    public enum DriveTypeEnum
    {
        Undefined = 0,
        /// <summary>
        /// Передний
        /// </summary>
        Front = 1,
        /// <summary>
        /// Задний
        /// </summary>
        Rear = 2,
        /// <summary>
        /// Полный
        /// </summary>
        Full = 3
    }
}