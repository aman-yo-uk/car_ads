﻿using Aman.CarAds.Dtos.Base.Impl;
using Aman.CarAds.Dtos.File;

namespace Aman.CarAds.Dtos.Ad
{
    public class AdFileDto : BaseDto
    {
        public long AdId { get; set; }
        public AdDto Ad { get; set; }
        public long FileId { get; set; }
        public FileDto File { get; set; }
        public bool IsModerated { get; set; }
    }
}