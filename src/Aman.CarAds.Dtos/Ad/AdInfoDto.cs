﻿namespace Aman.CarAds.Dtos.Ad
{
    public class AdInfoDto
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public AdStateEnum State { get; set; }
        public string StateName { get; set; }
        public long LocationId { get; set; }
        public string LocationName { get; set; }
        public long MarkId { get; set; }
        public string MarkName { get; set; }
        public long ModelId { get; set; }
        public string ModelName { get; set; }
        public string VinCode { get; set; }
        public CarStateEnum CarState { get; set; }
        public string CarStateName { get; set; }
        public bool IsCustomsCleared { get; set; }
        public long CarBodyId { get; set; }
        public string CarBodyName { get; set; }
        public EngineTypeEnum EngineType { get; set; }
        public string EngineTypeName { get; set; }
        public TransmissionTypeEnum TransmissionType { get; set; }
        public string TransmissionTypeName { get; set; }
        public SteeringWheelTypeEnum SteeringWheelType { get; set; }
        public string SteeringWheelTypeName { get; set; }
        public DriveTypeEnum DriveType { get; set; }
        public string DriveTypeName { get; set; }
        public int Mileage { get; set; }
        public AvailabilityTypeEnum AvailabilityType { get; set; }
        public string AvailabilityTypeName { get; set; }
        public decimal EngineVolume { get; set; }
        public long ColorId { get; set; }
        public string ColorName { get; set; }
        public int IssueYear { get; set; }
        public int Price { get; set; }
        public string Text { get; set; }
        public string DeactivateReason { get; set; }
    }
}