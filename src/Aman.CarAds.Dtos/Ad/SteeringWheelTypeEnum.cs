﻿namespace Aman.CarAds.Dtos.Ad
{
    /// <summary>
    /// Расположение руля
    /// </summary>
    public enum SteeringWheelTypeEnum
    {
        Undefined = 0,
        Left = 1,
        Right = 2
    }
}