﻿namespace Aman.CarAds.Dtos.Ad
{
    public enum EngineTypeEnum
    {
        Undefined = 0,
        /// <summary>
        /// бензин
        /// </summary>
        Petrol = 1,
        /// <summary>
        /// дизель
        /// </summary>
        Diesel = 2,
        /// <summary>
        /// газ-бензин
        /// </summary>
        GasPetrol = 3,
        /// <summary>
        /// газ
        /// </summary>
        Gas = 4,
        /// <summary>
        /// гибрид
        /// </summary>
        Hybrid = 5,
        /// <summary>
        /// электричество
        /// </summary>
        Electricity = 6
    }
}