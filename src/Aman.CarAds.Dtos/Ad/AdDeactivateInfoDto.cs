﻿namespace Aman.CarAds.Dtos.Ad
{
    public class AdDeactivateInfoDto
    {
        public long Id { get; set; }
        public string Reason { get; set; }
    }
}