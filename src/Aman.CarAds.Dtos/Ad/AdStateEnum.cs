﻿namespace Aman.CarAds.Dtos.Ad
{
    public enum AdStateEnum
    {
        Undefined = 0,
        /// <summary>
        /// На модерации
        /// </summary>
        Moderation = 1,
        /// <summary>
        /// Опубликован
        /// </summary>
        Published = 2,
        /// <summary>
        /// В архиве
        /// </summary>
        Archivated = 3,
        /// <summary>
        /// Не активно
        /// </summary>
        Deactivated = 4
    }
}