﻿using Aman.CarAds.Dtos.Base.Impl;
using Aman.CarAds.Dtos.Dir;
using Aman.CarAds.Dtos.User;

namespace Aman.CarAds.Dtos.Ad
{
    public class AdDto : BaseDto
    {
        public long UserId { get; set; }
        public UserDto User { get; set; }
        public AdStateEnum State { get; set; }
        public long LocationId { get; set; }
        public LocationDirDto Location { get; set; }
        public long ModelId { get; set; }
        public ModelDirDto Model { get; set; }
        public string VinCode { get; set; }
        public CarStateEnum CarState { get; set; }
        public bool IsCustomsCleared { get; set; }
        public long CarBodyId { get; set; }
        public CarBodyDirDto CarBody { get; set; }
        public EngineTypeEnum EngineType { get; set; }
        public TransmissionTypeEnum TransmissionType { get; set; }
        public SteeringWheelTypeEnum SteeringWheelType { get; set; }
        public DriveTypeEnum DriveType { get; set; }
        public int Mileage { get; set; }
        public AvailabilityTypeEnum AvailabilityType { get; set; }
        public decimal EngineVolume { get; set; }
        public long ColorId { get; set; }
        public ColorDirDto Color { get; set; }
        public int IssueYear { get; set; }
        public int Price { get; set; }
        public string Text { get; set; }
        public string DeactivateReason { get; set; }
    }
}