﻿namespace Aman.CarAds.Dtos.Ad
{
    public class AdListInfoDto
    {
        public long Id { get; set; }
        public string LocationName { get; set; }
        public string MarkName { get; set; }
        public string ModelName { get; set; }
        public string CarState { get; set; }
        public string CarBodyName { get; set; }
        public string EngineType { get; set; }
        public string TransmissionType { get; set; }
        public int Mileage { get; set; }
        public int Price { get; set; }
        public string Text { get; set; }
    }
}