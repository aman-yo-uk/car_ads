﻿using Aman.CarAds.Dtos.Base.Impl;

namespace Aman.CarAds.Dtos.Dir
{
    public class CarBodyDirDto : BaseDto
    {
        public string Name { get; set; }
    }
}