﻿using Aman.CarAds.Dtos.Base.Impl;

namespace Aman.CarAds.Dtos.Dir
{
    public class MarkDirDto : BaseDto
    {
        public string Name { get; set; }
    }
}