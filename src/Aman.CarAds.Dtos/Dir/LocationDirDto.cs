﻿using Aman.CarAds.Dtos.Base.Impl;

namespace Aman.CarAds.Dtos.Dir
{
    public class LocationDirDto : BaseDto
    {
        public long RegionDirId { get; set; }
        public RegionDirDto Region { get; set; }
        public string Name { get; set; }
    }
}