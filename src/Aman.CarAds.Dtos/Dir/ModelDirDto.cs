﻿using Aman.CarAds.Dtos.Base.Impl;

namespace Aman.CarAds.Dtos.Dir
{
    public class ModelDirDto : BaseDto
    {
        public long MarkId { get; set; }
        public MarkDirDto Mark { get; set; }
        public string Name { get; set; }
    }
}