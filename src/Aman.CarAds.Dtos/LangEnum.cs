﻿namespace Aman.CarAds.Dtos
{
    public enum LangEnum
    {
        Undefined,
        Kaz = 1,
        Rus = 2,
        Eng = 3
    }
}