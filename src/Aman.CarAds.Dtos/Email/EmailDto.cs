﻿using Aman.CarAds.Dtos.Base.Impl;
using Aman.CarAds.Dtos.User;

namespace Aman.CarAds.Dtos.Email
{
    public class EmailDto : BaseDto
    {
        public long UserId { get; set; }
        public UserDto User { get; set; }
        public string EmailAddress { get; set; }
        public bool IsConfirmed { get; set; }
    }
}