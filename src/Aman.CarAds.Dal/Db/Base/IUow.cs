﻿using Microsoft.EntityFrameworkCore;

namespace Aman.CarAds.Dal.Db.Base
{
    public interface IUow<TDbContext> where TDbContext : DbContext
    {
        TDbContext GetDbContext();
        IGenericTransaction BeginTransaction();
    }
}