﻿using Microsoft.EntityFrameworkCore;

namespace Aman.CarAds.Dal.Db.Base.Impl
{
    public class Uow<TDbContext> : IUow<TDbContext> where TDbContext : DbContext, new()
    {
        public TDbContext Db { get; private set; }

        public TDbContext GetDbContext()
        {
            return Db ??= new TDbContext();
        }

        public IGenericTransaction BeginTransaction()
        {
            return new SmartTransactionScope<TDbContext>(this);
            //return GetDbContext().Database.BeginTransaction();
        }
    }
}