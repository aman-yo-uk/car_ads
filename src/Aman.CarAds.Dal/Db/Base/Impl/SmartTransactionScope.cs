﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Data;
using System.Transactions;
using IsolationLevel = System.Transactions.IsolationLevel;

namespace Aman.CarAds.Dal.Db.Base.Impl
{
    public sealed class SmartTransactionScope<TDbContext> : IGenericTransaction
        where TDbContext : DbContext
    {
        private readonly IUow<TDbContext> _uow;
        private TransactionScope _transactionScope;

        public SmartTransactionScope(IUow<TDbContext> uow)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(uow));
            _transactionScope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted });
            var conn = uow.GetDbContext().Database.GetDbConnection();
            conn.EnlistTransaction(Transaction.Current);
        }

        public void Dispose()
        {
            _transactionScope?.Dispose();
            _transactionScope = null;

            var conn = _uow.GetDbContext().Database.GetDbConnection();
            try
            {
                conn.EnlistTransaction(null);
            }
            catch { }
        }

        public void Commit()
        {
            _transactionScope?.Complete();
        }

        public void Rollback()
        {
            throw new NotSupportedException("Явный вызов Rollback не поддерживается.");
        }

        public IDbCommand CreateDbCommand()
        {
            return _uow.GetDbContext().Database.GetDbConnection().CreateCommand();
        }
    }
}