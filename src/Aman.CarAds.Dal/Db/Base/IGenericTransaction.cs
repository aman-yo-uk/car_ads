﻿using System;
using System.Data;

namespace Aman.CarAds.Dal.Db.Base
{
    public interface IGenericTransaction : IDisposable
    {
        void Commit();
        void Rollback();

        IDbCommand CreateDbCommand();
    }
}