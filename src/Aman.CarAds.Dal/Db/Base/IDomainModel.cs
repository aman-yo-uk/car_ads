﻿namespace Aman.CarAds.Dal.Db.Base
{
    public interface IDomainModel
    {
        long Id { get; set; }
        bool IsDeleted { get; set; }
    }
}