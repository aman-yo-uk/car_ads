﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Aman.CarAds.Dal.Db.Base
{
    public interface ISimpleRepository<TDomainModel, TDto>
    {
        IQueryable<TDto> GetDtoQuery(bool getDeleted = false);
        TDto GetDtoById(long id);
        Task<TDto> GetDtoByIdAsync(long id);
        long SaveDto(TDto dto);
        Task<long> SaveDtoAsync(TDto dto);
        bool DeleteDto(TDto dto);
        Task<bool> DeleteDtoAsync(TDto dto);
        Expression<Func<TDomainModel, TDto>> ToDto();
        Func<TDto, TDomainModel, TDomainModel> ToDomainModel();
    }
}