﻿using Aman.CarAds.Dal.Db.Base;
using System;
using System.Collections.Generic;

namespace Aman.CarAds.Dal.Db.CarAds.Models
{
    public class User : IDomainModel
    {
        public long Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Login { get; set; }
        public string SecureKeyHash { get; set; }
        public byte Type { get; set; }
        public string Nickname { get; set; }
        public DateTime? BirthDate { get; set; }
        public byte GenderType { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime? BannedTo { get; set; }
        public string BannedReason { get; set; }
        public List<Ad> Ads { get; set; }
        public List<Email> Emails { get; set; }
        public List<Phone> Phones { get; set; }
        public List<ModerationHistory> ModerationHistories { get; set; }
    }
}