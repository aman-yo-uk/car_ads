﻿using Aman.CarAds.Dal.Db.Base;
using System;

namespace Aman.CarAds.Dal.Db.CarAds.Models
{
    public class Email : IDomainModel
    {
        public long Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public long UserId { get; set; }
        public User User { get; set; }
        public string EmailAddress { get; set; }
        public bool IsConfirmed { get; set; }
    }
}