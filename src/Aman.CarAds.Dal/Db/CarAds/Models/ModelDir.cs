﻿using Aman.CarAds.Dal.Db.Base;
using System;
using System.Collections.Generic;

namespace Aman.CarAds.Dal.Db.CarAds.Models
{
    public class ModelDir : IDomainModel
    {
        public long Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public long MarkId { get; set; }
        public MarkDir Mark { get; set; }
        public string Name { get; set; }
        public List<Ad> Ads { get; set; }
    }
}