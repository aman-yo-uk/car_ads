﻿using Aman.CarAds.Dal.Db.Base;
using System;

namespace Aman.CarAds.Dal.Db.CarAds.Models
{
    public class ModerationHistory : IDomainModel
    {
        public long Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public long UserId { get; set; }
        public User User { get; set; }
        public long AdId { get; set; }
        public Ad Ad { get; set; }
        public byte DecisionType { get; set; }
        public string RejectReason { get; set; }
    }
}