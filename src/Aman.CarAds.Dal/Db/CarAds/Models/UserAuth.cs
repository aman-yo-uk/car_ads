﻿using Aman.CarAds.Dal.Db.Base;
using System;

namespace Aman.CarAds.Dal.Db.CarAds.Models
{
    public class UserAuth : IDomainModel
    {
        public long Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public long UserId { get; set; }
        public User User { get; set; }
        public DateTime LoginDateTime { get; set; }
        public string Token { get; set; }
        public string IpAddress { get; set; }
    }
}