﻿using Aman.CarAds.Dal.Db.Base;
using System;
using System.Collections.Generic;

namespace Aman.CarAds.Dal.Db.CarAds.Models
{
    public class File : IDomainModel
    {
        public long Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Name { get; set; }
        public string MimeType { get; set; }
        public byte[] Data { get; set; }
        public List<AdFile> AdFiles { get; set; }
    }
}