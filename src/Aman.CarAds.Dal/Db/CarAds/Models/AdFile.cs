﻿using Aman.CarAds.Dal.Db.Base;
using System;

namespace Aman.CarAds.Dal.Db.CarAds.Models
{
    public class AdFile : IDomainModel
    {
        public long Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public long AdId { get; set; }
        public Ad Ad { get; set; }
        public long FileId { get; set; }
        public File File { get; set; }
        public bool IsModerated { get; set; }
    }
}