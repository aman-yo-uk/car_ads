﻿using Aman.CarAds.Dal.Db.Base;
using System;
using System.Collections.Generic;

namespace Aman.CarAds.Dal.Db.CarAds.Models
{
    public class Ad : IDomainModel
    {
        public long Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public long UserId { get; set; }
        public User User { get; set; }
        public byte State { get; set; }
        public long LocationId { get; set; }
        public LocationDir Location { get; set; }
        public long ModelId { get; set; }
        public ModelDir Model { get; set; }
        public string VinCode { get; set; }
        public byte CarState { get; set; }
        public bool IsCustomsCleared { get; set; }
        public long CarBodyId { get; set; }
        public CarBodyDir CarBody { get; set; }
        public byte EngineType { get; set; }
        public byte TransmissionType { get; set; }
        public byte SteeringWheelType { get; set; }
        public byte DriveType { get; set; }
        public int Mileage { get; set; }
        public byte AvailabilityType { get; set; }
        public decimal EngineVolume { get; set; }
        public long ColorId { get; set; }
        public ColorDir Color { get; set; }
        public int IssueYear { get; set; }
        public int Price { get; set; }
        public string Text { get; set; }
        public string DeactivateReason { get; set; }
        public List<AdFile> AdFiles { get; set; }
        public List<ModerationHistory> ModerationHistories { get; set; }
    }
}