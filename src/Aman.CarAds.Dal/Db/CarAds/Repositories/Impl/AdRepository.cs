﻿using Aman.CarAds.Core.Resources;
using Aman.CarAds.Dal.Db.Base;
using Aman.CarAds.Dal.Db.Base.Impl;
using Aman.CarAds.Dal.Db.CarAds.Models;
using Aman.CarAds.Dtos.Ad;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Aman.CarAds.Dal.Db.CarAds.Repositories.Impl
{
    public class AdRepository : SimpleRepository<CarAdsContext, Ad, AdDto>, IAdRepository
    {
        private readonly IUow<CarAdsContext> _uow;

        public AdRepository(IUow<CarAdsContext> uow) : base(uow)
        {
            _uow = uow;
        }

        public override Func<AdDto, Ad, Ad> ToDomainModel()
        {
            return (_, source) => new Ad
            {
                Id = source != null ? source.Id : _.Id,
                UserId = _.UserId,
                State = (byte)_.State,
                LocationId = _.LocationId,
                ModelId = _.ModelId,
                VinCode = _.VinCode,
                CarState = (byte)_.CarState,
                IsCustomsCleared = _.IsCustomsCleared,
                CarBodyId = _.CarBodyId,
                EngineType = (byte)_.EngineType,
                TransmissionType = (byte)_.TransmissionType,
                SteeringWheelType = (byte)_.SteeringWheelType,
                DriveType = (byte)_.DriveType,
                Mileage = _.Mileage,
                AvailabilityType = (byte)_.AvailabilityType,
                EngineVolume = _.EngineVolume,
                ColorId = _.ColorId,
                IssueYear = _.IssueYear,
                Price = _.Price,
                Text = _.Text,
                DeactivateReason = _.DeactivateReason
            };
        }

        public override Expression<Func<Ad, AdDto>> ToDto()
        {
            return _ => new AdDto
            {
                Id = _.Id,
                UserId = _.UserId,
                State = (AdStateEnum)_.State,
                LocationId = _.LocationId,
                ModelId = _.ModelId,
                VinCode = _.VinCode,
                CarState = (CarStateEnum)_.CarState,
                IsCustomsCleared = _.IsCustomsCleared,
                CarBodyId = _.CarBodyId,
                EngineType = (EngineTypeEnum)_.EngineType,
                TransmissionType = (TransmissionTypeEnum)_.TransmissionType,
                SteeringWheelType = (SteeringWheelTypeEnum)_.SteeringWheelType,
                DriveType = (DriveTypeEnum)_.DriveType,
                Mileage = _.Mileage,
                AvailabilityType = (AvailabilityTypeEnum)_.AvailabilityType,
                EngineVolume = _.EngineVolume,
                ColorId = _.ColorId,
                IssueYear = _.IssueYear,
                Price = _.Price,
                Text = _.Text,
                DeactivateReason = _.DeactivateReason
            };
        }

        public IQueryable<AdFullInfoDto> GetAdFullInfos(string culture, bool getDeleted = false)
        {
            return from a in _uow.GetDbContext().Ads
                   join l in _uow.GetDbContext().LocationDirs on a.LocationId equals l.Id
                   join mo in _uow.GetDbContext().ModelDirs on a.ModelId equals mo.Id
                   join ma in _uow.GetDbContext().MarkDirs on mo.MarkId equals ma.Id
                   join b in _uow.GetDbContext().CarBodyDirs on a.CarBodyId equals b.Id
                   join c in _uow.GetDbContext().ColorDirs on a.ColorId equals c.Id
                   where getDeleted || !a.IsDeleted
                   select new AdFullInfoDto
                   {
                       Id = a.Id,
                       UserId = a.UserId,
                       State = (AdStateEnum)a.State,
                       StateName = ResourceHelper.GetEnumDirResource((AdStateEnum)a.State, culture),
                       LocationId = a.LocationId,
                       LocationName = l.Name,
                       MarkId = mo.MarkId,
                       MarkName = ma.Name,
                       ModelId = a.ModelId,
                       ModelName = mo.Name,
                       VinCode = a.VinCode,
                       CarState = (CarStateEnum)a.CarState,
                       CarStateName = ResourceHelper.GetEnumDirResource((CarStateEnum)a.CarState, culture),
                       IsCustomsCleared = a.IsCustomsCleared,
                       CarBodyId = a.CarBodyId,
                       CarBodyName = b.Name,
                       EngineType = (EngineTypeEnum)a.EngineType,
                       EngineTypeName = ResourceHelper.GetEnumDirResource((EngineTypeEnum)a.EngineType, culture),
                       TransmissionType = (TransmissionTypeEnum)a.TransmissionType,
                       TransmissionTypeName = ResourceHelper.GetEnumDirResource((TransmissionTypeEnum)a.TransmissionType, culture),
                       SteeringWheelType = (SteeringWheelTypeEnum)a.SteeringWheelType,
                       SteeringWheelTypeName = ResourceHelper.GetEnumDirResource((SteeringWheelTypeEnum)a.SteeringWheelType, culture),
                       DriveType = (DriveTypeEnum)a.DriveType,
                       DriveTypeName = ResourceHelper.GetEnumDirResource((DriveTypeEnum)a.DriveType, culture),
                       Mileage = a.Mileage,
                       AvailabilityType = (AvailabilityTypeEnum)a.AvailabilityType,
                       AvailabilityTypeName = ResourceHelper.GetEnumDirResource((AvailabilityTypeEnum)a.AvailabilityType, culture),
                       EngineVolume = a.EngineVolume,
                       ColorId = a.ColorId,
                       ColorName = c.Name,
                       IssueYear = a.IssueYear,
                       Price = a.Price,
                       Text = a.Text,
                       DeactivateReason = a.DeactivateReason
                   };
        }
    }
}