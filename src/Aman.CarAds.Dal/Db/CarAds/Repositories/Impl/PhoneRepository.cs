﻿using Aman.CarAds.Dal.Db.Base;
using Aman.CarAds.Dal.Db.Base.Impl;
using Aman.CarAds.Dal.Db.CarAds.Models;
using Aman.CarAds.Dtos.Phone;
using System;
using System.Linq.Expressions;

namespace Aman.CarAds.Dal.Db.CarAds.Repositories.Impl
{
    public class PhoneRepository : SimpleRepository<CarAdsContext, Phone, PhoneDto>, IPhoneRepository
    {
        private readonly IUow<CarAdsContext> _uow;

        public PhoneRepository(IUow<CarAdsContext> uow) : base(uow)
        {
            _uow = uow;
        }

        public override Func<PhoneDto, Phone, Phone> ToDomainModel()
        {
            return (_, source) => new Phone
            {
                Id = source != null ? source.Id : _.Id,
                UserId = _.UserId,
                PhoneNumber = _.PhoneNumber,
                IsConfirmed = _.IsConfirmed
            };
        }

        public override Expression<Func<Phone, PhoneDto>> ToDto()
        {
            return _ => new PhoneDto
            {
                Id = _.Id,
                UserId = _.UserId,
                PhoneNumber = _.PhoneNumber,
                IsConfirmed = _.IsConfirmed
            };
        }
    }
}