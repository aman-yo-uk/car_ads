﻿using Aman.CarAds.Dal.Db.Base;
using Aman.CarAds.Dal.Db.Base.Impl;
using Aman.CarAds.Dal.Db.CarAds.Models;
using Aman.CarAds.Dtos.File;
using System;
using System.Linq.Expressions;

namespace Aman.CarAds.Dal.Db.CarAds.Repositories.Impl
{
    public class FileRepository : SimpleRepository<CarAdsContext, File, FileDto>, IFileRepository
    {
        private readonly IUow<CarAdsContext> _uow;

        public FileRepository(IUow<CarAdsContext> uow) : base(uow)
        {
            _uow = uow;
        }

        public override Func<FileDto, File, File> ToDomainModel()
        {
            return (_, source) => new File
            {
                Id = source != null ? source.Id : _.Id,
                Name = _.Name,
                MimeType = _.MimeType,
                Data = _.Data
            };
        }

        public override Expression<Func<File, FileDto>> ToDto()
        {
            return _ => new FileDto
            {
                Id = _.Id,
                Name = _.Name,
                MimeType = _.MimeType,
                Data = _.Data
            };
        }
    }
}