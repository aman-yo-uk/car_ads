﻿using Aman.CarAds.Dal.Db.Base;
using Aman.CarAds.Dal.Db.Base.Impl;
using Aman.CarAds.Dal.Db.CarAds.Models;
using Aman.CarAds.Dtos.Dir;
using System;
using System.Linq.Expressions;

namespace Aman.CarAds.Dal.Db.CarAds.Repositories.Impl
{
    public class LocationDirRepository : SimpleRepository<CarAdsContext, LocationDir, LocationDirDto>, ILocationDirRepository
    {
        private readonly IUow<CarAdsContext> _uow;

        public LocationDirRepository(IUow<CarAdsContext> uow) : base(uow)
        {
            _uow = uow;
        }

        public override Func<LocationDirDto, LocationDir, LocationDir> ToDomainModel()
        {
            return (_, source) => new LocationDir
            {
                Id = source != null ? source.Id : _.Id,
                RegionDirId = _.RegionDirId,
                Name = _.Name
            };
        }

        public override Expression<Func<LocationDir, LocationDirDto>> ToDto()
        {
            return _ => new LocationDirDto
            {
                Id = _.Id,
                RegionDirId = _.RegionDirId,
                Name = _.Name
            };
        }
    }
}