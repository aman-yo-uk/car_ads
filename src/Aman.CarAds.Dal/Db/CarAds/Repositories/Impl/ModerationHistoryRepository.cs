﻿using Aman.CarAds.Dal.Db.Base;
using Aman.CarAds.Dal.Db.Base.Impl;
using Aman.CarAds.Dal.Db.CarAds.Models;
using Aman.CarAds.Dtos.ModerationHistory;
using System;
using System.Linq.Expressions;

namespace Aman.CarAds.Dal.Db.CarAds.Repositories.Impl
{
    public class ModerationHistoryRepository : SimpleRepository<CarAdsContext, ModerationHistory, ModerationHistoryDto>, IModerationHistoryRepository
    {
        private readonly IUow<CarAdsContext> _uow;

        public ModerationHistoryRepository(IUow<CarAdsContext> uow) : base(uow)
        {
            _uow = uow;
        }

        public override Func<ModerationHistoryDto, ModerationHistory, ModerationHistory> ToDomainModel()
        {
            return (_, source) => new ModerationHistory
            {
                Id = source != null ? source.Id : _.Id,
                UserId = _.UserId,
                AdId = _.AdId,
                DecisionType = (byte)_.DecisionType,
                RejectReason = _.RejectReason
            };
        }

        public override Expression<Func<ModerationHistory, ModerationHistoryDto>> ToDto()
        {
            return _ => new ModerationHistoryDto
            {
                Id = _.Id,
                UserId = _.UserId,
                AdId = _.AdId,
                DecisionType = (DecisionTypeEnum)_.DecisionType,
                RejectReason = _.RejectReason
            };
        }
    }
}