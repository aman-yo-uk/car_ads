﻿using Aman.CarAds.Dal.Db.Base;
using Aman.CarAds.Dal.Db.Base.Impl;
using Aman.CarAds.Dal.Db.CarAds.Models;
using Aman.CarAds.Dtos.User;
using System;
using System.Linq.Expressions;

namespace Aman.CarAds.Dal.Db.CarAds.Repositories.Impl
{
    public class UserRepository : SimpleRepository<CarAdsContext, User, UserDto>, IUserRepository
    {
        private readonly IUow<CarAdsContext> _uow;

        public UserRepository(IUow<CarAdsContext> uow) : base(uow)
        {
            _uow = uow;
        }

        public override Func<UserDto, User, User> ToDomainModel()
        {
            return (_, source) => new User
            {
                Id = source != null ? source.Id : _.Id,
                Login = _.Login,
                SecureKeyHash = _.SecureKeyHash,
                Type = (byte)_.Type,
                Nickname = _.Nickname,
                BirthDate = _.BirthDate,
                GenderType = (byte)_.GenderType,
                RegistrationDate = _.RegistrationDate,
                BannedTo = _.BannedTo,
                BannedReason = _.BannedReason
            };
        }

        public override Expression<Func<User, UserDto>> ToDto()
        {
            return _ => new UserDto
            {
                Id = _.Id,
                Login = _.Login,
                SecureKeyHash = _.SecureKeyHash,
                Type = (UserTypeEnum)_.Type,
                Nickname = _.Nickname,
                BirthDate = _.BirthDate,
                GenderType = (GenderTypeEnum)_.GenderType,
                RegistrationDate = _.RegistrationDate,
                BannedTo = _.BannedTo,
                BannedReason = _.BannedReason
            };
        }
    }
}