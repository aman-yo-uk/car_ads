﻿using Aman.CarAds.Dal.Db.Base;
using Aman.CarAds.Dal.Db.Base.Impl;
using Aman.CarAds.Dal.Db.CarAds.Models;
using Aman.CarAds.Dtos.Ad;
using System;
using System.Linq.Expressions;

namespace Aman.CarAds.Dal.Db.CarAds.Repositories.Impl
{
    public class AdFileRepository : SimpleRepository<CarAdsContext, AdFile, AdFileDto>, IAdFileRepository
    {
        private readonly IUow<CarAdsContext> _uow;

        public AdFileRepository(IUow<CarAdsContext> uow) : base(uow)
        {
            _uow = uow;
        }

        public override Func<AdFileDto, AdFile, AdFile> ToDomainModel()
        {
            return (_, source) => new AdFile
            {
                Id = source != null ? source.Id : _.Id,
                AdId = _.AdId,
                FileId = _.FileId,
                IsModerated = _.IsModerated
            };
        }

        public override Expression<Func<AdFile, AdFileDto>> ToDto()
        {
            return _ => new AdFileDto
            {
                Id = _.Id,
                AdId = _.AdId,
                FileId = _.FileId,
                IsModerated = _.IsModerated
            };
        }
    }
}