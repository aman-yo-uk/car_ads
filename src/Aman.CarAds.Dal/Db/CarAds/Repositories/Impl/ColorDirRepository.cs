﻿using Aman.CarAds.Dal.Db.Base;
using Aman.CarAds.Dal.Db.Base.Impl;
using Aman.CarAds.Dal.Db.CarAds.Models;
using Aman.CarAds.Dtos.Dir;
using System;
using System.Linq.Expressions;

namespace Aman.CarAds.Dal.Db.CarAds.Repositories.Impl
{
    public class ColorDirRepository : SimpleRepository<CarAdsContext, ColorDir, ColorDirDto>, IColorDirRepository
    {
        private readonly IUow<CarAdsContext> _uow;

        public ColorDirRepository(IUow<CarAdsContext> uow) : base(uow)
        {
            _uow = uow;
        }

        public override Func<ColorDirDto, ColorDir, ColorDir> ToDomainModel()
        {
            return (_, source) => new ColorDir
            {
                Id = source != null ? source.Id : _.Id,
                Name = _.Name
            };
        }

        public override Expression<Func<ColorDir, ColorDirDto>> ToDto()
        {
            return _ => new ColorDirDto
            {
                Id = _.Id,
                Name = _.Name
            };
        }
    }
}