﻿using Aman.CarAds.Dal.Db.Base;
using Aman.CarAds.Dal.Db.Base.Impl;
using Aman.CarAds.Dal.Db.CarAds.Models;
using Aman.CarAds.Dtos.Dir;
using System;
using System.Linq.Expressions;

namespace Aman.CarAds.Dal.Db.CarAds.Repositories.Impl
{
    public class RegionDirRepository : SimpleRepository<CarAdsContext, RegionDir, RegionDirDto>, IRegionDirRepository
    {
        private readonly IUow<CarAdsContext> _uow;

        public RegionDirRepository(IUow<CarAdsContext> uow) : base(uow)
        {
            _uow = uow;
        }

        public override Func<RegionDirDto, RegionDir, RegionDir> ToDomainModel()
        {
            return (_, source) => new RegionDir
            {
                Id = source != null ? source.Id : _.Id,
                Name = _.Name
            };
        }

        public override Expression<Func<RegionDir, RegionDirDto>> ToDto()
        {
            return _ => new RegionDirDto
            {
                Id = _.Id,
                Name = _.Name
            };
        }
    }
}