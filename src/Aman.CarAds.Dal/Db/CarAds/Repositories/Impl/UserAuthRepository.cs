﻿using Aman.CarAds.Dal.Db.Base;
using Aman.CarAds.Dal.Db.Base.Impl;
using Aman.CarAds.Dal.Db.CarAds.Models;
using Aman.CarAds.Dtos.UserAuth;
using System;
using System.Linq.Expressions;

namespace Aman.CarAds.Dal.Db.CarAds.Repositories.Impl
{
    public class UserAuthRepository : SimpleRepository<CarAdsContext, UserAuth, UserAuthDto>, IUserAuthRepository
    {
        private readonly IUow<CarAdsContext> _uow;

        public UserAuthRepository(IUow<CarAdsContext> uow) : base(uow)
        {
            _uow = uow;
        }

        public override Func<UserAuthDto, UserAuth, UserAuth> ToDomainModel()
        {
            return (_, source) => new UserAuth
            {
                Id = source != null ? source.Id : _.Id,
                UserId = _.UserId,
                LoginDateTime = _.LoginDateTime,
                Token = _.Token,
                IpAddress = _.IpAddress
            };
        }

        public override Expression<Func<UserAuth, UserAuthDto>> ToDto()
        {
            return _ => new UserAuthDto
            {
                Id = _.Id,
                UserId = _.UserId,
                LoginDateTime = _.LoginDateTime,
                Token = _.Token,
                IpAddress = _.IpAddress
            };
        }
    }
}