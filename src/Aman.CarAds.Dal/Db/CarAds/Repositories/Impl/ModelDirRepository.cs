﻿using Aman.CarAds.Dal.Db.Base;
using Aman.CarAds.Dal.Db.Base.Impl;
using Aman.CarAds.Dal.Db.CarAds.Models;
using Aman.CarAds.Dtos.Dir;
using System;
using System.Linq.Expressions;

namespace Aman.CarAds.Dal.Db.CarAds.Repositories.Impl
{
    public class ModelDirRepository : SimpleRepository<CarAdsContext, ModelDir, ModelDirDto>, IModelDirRepository
    {
        private readonly IUow<CarAdsContext> _uow;

        public ModelDirRepository(IUow<CarAdsContext> uow) : base(uow)
        {
            _uow = uow;
        }

        public override Func<ModelDirDto, ModelDir, ModelDir> ToDomainModel()
        {
            return (_, source) => new ModelDir
            {
                Id = source != null ? source.Id : _.Id,
                MarkId = _.MarkId,
                Name = _.Name
            };
        }

        public override Expression<Func<ModelDir, ModelDirDto>> ToDto()
        {
            return _ => new ModelDirDto
            {
                Id = _.Id,
                MarkId = _.MarkId,
                Name = _.Name
            };
        }
    }
}