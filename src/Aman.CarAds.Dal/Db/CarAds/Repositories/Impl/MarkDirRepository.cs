﻿using Aman.CarAds.Dal.Db.Base;
using Aman.CarAds.Dal.Db.Base.Impl;
using Aman.CarAds.Dal.Db.CarAds.Models;
using Aman.CarAds.Dtos.Dir;
using System;
using System.Linq.Expressions;

namespace Aman.CarAds.Dal.Db.CarAds.Repositories.Impl
{
    public class MarkDirRepository : SimpleRepository<CarAdsContext, MarkDir, MarkDirDto>, IMarkDirRepository
    {
        private readonly IUow<CarAdsContext> _uow;

        public MarkDirRepository(IUow<CarAdsContext> uow) : base(uow)
        {
            _uow = uow;
        }

        public override Func<MarkDirDto, MarkDir, MarkDir> ToDomainModel()
        {
            return (_, source) => new MarkDir
            {
                Id = source != null ? source.Id : _.Id,
                Name = _.Name
            };
        }

        public override Expression<Func<MarkDir, MarkDirDto>> ToDto()
        {
            return _ => new MarkDirDto
            {
                Id = _.Id,
                Name = _.Name
            };
        }
    }
}