﻿using Aman.CarAds.Dal.Db.Base;
using Aman.CarAds.Dal.Db.Base.Impl;
using Aman.CarAds.Dal.Db.CarAds.Models;
using Aman.CarAds.Dtos.Dir;
using System;
using System.Linq.Expressions;

namespace Aman.CarAds.Dal.Db.CarAds.Repositories.Impl
{
    public class CarBodyDirRepository : SimpleRepository<CarAdsContext, CarBodyDir, CarBodyDirDto>, ICarBodyDirRepository
    {
        private readonly IUow<CarAdsContext> _uow;

        public CarBodyDirRepository(IUow<CarAdsContext> uow) : base(uow)
        {
            _uow = uow;
        }

        public override Func<CarBodyDirDto, CarBodyDir, CarBodyDir> ToDomainModel()
        {
            return (_, source) => new CarBodyDir
            {
                Id = source != null ? source.Id : _.Id,
                Name = _.Name
            };
        }

        public override Expression<Func<CarBodyDir, CarBodyDirDto>> ToDto()
        {
            return _ => new CarBodyDirDto
            {
                Id = _.Id,
                Name = _.Name
            };
        }
    }
}