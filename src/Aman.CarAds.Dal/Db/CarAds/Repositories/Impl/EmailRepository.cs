﻿using Aman.CarAds.Dal.Db.Base;
using Aman.CarAds.Dal.Db.Base.Impl;
using Aman.CarAds.Dal.Db.CarAds.Models;
using Aman.CarAds.Dtos.Email;
using System;
using System.Linq.Expressions;

namespace Aman.CarAds.Dal.Db.CarAds.Repositories.Impl
{
    public class EmailRepository : SimpleRepository<CarAdsContext, Email, EmailDto>, IEmailRepository
    {
        private readonly IUow<CarAdsContext> _uow;

        public EmailRepository(IUow<CarAdsContext> uow) : base(uow)
        {
            _uow = uow;
        }

        public override Func<EmailDto, Email, Email> ToDomainModel()
        {
            return (_, source) => new Email
            {
                Id = source != null ? source.Id : _.Id,
                UserId = _.UserId,
                EmailAddress = _.EmailAddress,
                IsConfirmed = _.IsConfirmed
            };
        }

        public override Expression<Func<Email, EmailDto>> ToDto()
        {
            return _ => new EmailDto
            {
                Id = _.Id,
                UserId = _.UserId,
                EmailAddress = _.EmailAddress,
                IsConfirmed = _.IsConfirmed
            };
        }
    }
}