﻿using Aman.CarAds.Dal.Db.Base;
using Aman.CarAds.Dal.Db.CarAds.Models;
using Aman.CarAds.Dtos.Email;

namespace Aman.CarAds.Dal.Db.CarAds.Repositories
{
    public interface IEmailRepository : ISimpleRepository<Email, EmailDto>
    {

    }
}