﻿using Aman.CarAds.Dal.Db.Base;
using Aman.CarAds.Dal.Db.CarAds.Models;
using Aman.CarAds.Dtos.Ad;
using System.Linq;

namespace Aman.CarAds.Dal.Db.CarAds.Repositories
{
    public interface IAdRepository : ISimpleRepository<Ad, AdDto>
    {
        IQueryable<AdFullInfoDto> GetAdFullInfos(string culture, bool getDeleted = false);
    }
}