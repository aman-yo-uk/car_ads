﻿using Aman.CarAds.Dal.Db.Base;
using Aman.CarAds.Dal.Db.CarAds.Models;
using Aman.CarAds.Dtos.File;

namespace Aman.CarAds.Dal.Db.CarAds.Repositories
{
    public interface IFileRepository : ISimpleRepository<File, FileDto>
    {

    }
}