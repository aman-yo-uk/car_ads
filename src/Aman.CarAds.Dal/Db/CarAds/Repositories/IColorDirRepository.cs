﻿using Aman.CarAds.Dal.Db.Base;
using Aman.CarAds.Dal.Db.CarAds.Models;
using Aman.CarAds.Dtos.Dir;

namespace Aman.CarAds.Dal.Db.CarAds.Repositories
{
    public interface IColorDirRepository : ISimpleRepository<ColorDir, ColorDirDto>
    {

    }
}