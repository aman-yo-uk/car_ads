﻿using Aman.CarAds.Dal.Db.Base;
using Aman.CarAds.Dal.Db.CarAds.Models;
using Aman.CarAds.Dtos.ModerationHistory;

namespace Aman.CarAds.Dal.Db.CarAds.Repositories
{
    public interface IModerationHistoryRepository : ISimpleRepository<ModerationHistory, ModerationHistoryDto>
    {

    }
}