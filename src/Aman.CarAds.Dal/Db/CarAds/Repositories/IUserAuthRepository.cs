﻿using Aman.CarAds.Dal.Db.Base;
using Aman.CarAds.Dal.Db.CarAds.Models;
using Aman.CarAds.Dtos.UserAuth;

namespace Aman.CarAds.Dal.Db.CarAds.Repositories
{
    public interface IUserAuthRepository : ISimpleRepository<UserAuth, UserAuthDto>
    {

    }
}